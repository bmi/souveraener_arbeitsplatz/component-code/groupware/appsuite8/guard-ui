define([
    'pgp_mail/toggle-encryption'
], function (ToggleEncryption) {
    describe('Toggle Encryption Button', function () {
        describe('View', function () {
            //var deviceStub;
            beforeEach(function () {
                //deviceStub = sinon.stub(_, 'device').withArgs('small').returns(false);
                window.oxguarddata.settings.oxguard = true;
            });
            afterEach(function () {
                _.device.restore();
            });

            it('should render an icon', function () {
                var v = new ToggleEncryption.View({
                    model: new Backbone.Model()
                });
                v.render();
                expect(v.$('i.fa').length, 'one font-awesome icon found').to.equal(1);
                expect(v.$('.encrypted').length, 'no encrypted class found').to.equal(0);
            });

            it('should add encryption class if encrypted', function () {
                var v = new ToggleEncryption.View({
                    model: new Backbone.Model({
                        encrypt: true
                    })
                });
                v.render();
                expect(v.$('i.fa').length, 'one font-awesome icon found').to.equal(1);
                expect(v.$('.encrypted').length, 'encrypted class found').to.equal(1);
            });

            it('should toggle encryption class if model changes', function () {
                var v = new ToggleEncryption.View({
                    model: new Backbone.Model()
                });
                v.render();
                expect(v.$('i.fa').length, 'one font-awesome icon found').to.equal(1);
                expect(v.$('.encrypted').length, 'encrypted class found').to.equal(0);
                v.model.set('encrypt', true);
                expect(v.$('i.fa').length, 'one font-awesome icon found').to.equal(1);
                expect(v.$('.encrypted').length, 'encrypted class found').to.equal(1);
            });

            it('should toggle encryption class on click', function () {
                var v = new ToggleEncryption.View({
                    model: new Backbone.Model()
                });
                v.render();
                expect(v.$('i.fa').length, 'one font-awesome icon found').to.equal(1);
                expect(v.$('.encrypted').length, 'encrypted class found').to.equal(0);
                v.$('i.fa').click();
                expect(v.$('i.fa').length, 'one font-awesome icon found').to.equal(1);
                expect(v.$('.encrypted').length, 'encrypted class found').to.equal(1);
            });

            it('should not toggle encryption if forceEncryption option is set true', function () {
                var v = new ToggleEncryption.View({
                    model: new Backbone.Model()
                });

                expect(v.model.get('encrypt')).not.to.be.defined;
                v.forceEncryption();
                v.render();

                expect(v.model.get('encrypt')).to.be.true;
                expect(v.$('.encrypted').length, 'encrypted class found').to.equal(1);
                v.$('i.fa').click();
                expect(v.model.get('encrypt')).to.be.true;
                expect(v.$('.encrypted').length, 'encrypted class found').to.equal(1);
            });
        });
    });
});
