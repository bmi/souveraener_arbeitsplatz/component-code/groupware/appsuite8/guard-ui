define([
    'oxguard/settings/defaults',
    'spec/shared/capabilities'
], function (defaults, caputil) {
    'use strict';

    var capabilities = caputil.preset('common').init(['oxguard/settings/defaults'], [defaults]);

    describe('Guard settings', function () {
        describe('cryptoProvider defaults', function () {
            it('with cap=guard,-mailvelope,-forceMailvelope (guard only scenario)', function () {
                return capabilities
                    .enable(['guard'])
                    .then(function () {
                        return capabilities.disable(['mailvelope', 'forceMailvelope']);
                    })
                    .then(function () {
                        expect(defaults.cryptoProvider).to.equal('guard');
                    });
            });

            it('with cap=-guard,mailvelope,-forceMailvelope (mailvelope only scenario)', function () {
                return capabilities
                    .enable(['mailvelope'])
                    .then(function () {
                        return capabilities.disable(['guard', 'forceMailvelope']);
                    })
                    .then(function () {
                        expect(defaults.cryptoProvider).to.equal('mailvelope');
                    });
            });

            it('with cap=guard,mailvelope,forceMailvelope (admin enforces mailvelope usage scenario)', function () {
                return capabilities
                    .enable(['guard', 'mailvelope', 'forceMailvelope'])
                    .then(function () {
                        return capabilities.disable([]);
                    })
                    .then(function () {
                        expect(defaults.cryptoProvider).to.equal('mailvelope');
                    });
            });

            it('with cap=-guard,-mailvelope,-forceMailvelope (everything disabled scenario)', function () {
                return capabilities
                    .enable([])
                    .then(function () {
                        return capabilities.disable(['guard', 'mailvelope', 'forceMailvelope']);
                    })
                    .then(function () {
                        expect(defaults.cryptoProvider).not.to.be.defined;
                    });
            });

            it('with cap=guard,mailvelope,-forceMailvelope (user must choose scenario)', function () {
                return capabilities
                    .enable(['guard', 'mailvelope'])
                    .then(function () {
                        return capabilities.disable(['forceMailvelope']);
                    })
                    .then(function () {
                        expect(defaults.cryptoProvider).not.to.be.defined;
                    });
            });
        });
    });
});
