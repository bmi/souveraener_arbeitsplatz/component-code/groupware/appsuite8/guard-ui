/**
 * All content on this website (including text, images, source
 * code and any other original works), unless otherwise noted,
 * is licensed under a Creative Commons License.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 *
 * Copyright (C) 2016-2020 OX Software GmbH
 */

import coreUtil from '@/io.ox/guard/util'
import _ from '$/underscore'

const util = _.extend({}, coreUtil)

util.encryptedOnly = function encryptedOnly (list) {
  if (list === null) return (false)
  if (list instanceof Array) {
    return list.reduce(function (acc, file) {
      return acc && file.isEncrypted()
    }, true)
  } return (list.isEncrypted())
}

util.hasEncrypted = function hasEncryptd (list) {
  if (list === null) return (false)
  if (list instanceof Array) {
    let encrypted = false
    list.forEach(function (file) {
      if (file.isEncrypted()) encrypted = true
    })
    return (encrypted)
  } return (list.isEncrypted())
}

util.isNotAttachment = function isNotAttachment (list) {
  if (list === null) return (false)
  if (list instanceof Array) {
    return list.reduce(function (acc, file) {
      return acc && (file.pgpFormat === undefined)
    }, true)
  } return (list.get('pgpFormat') === undefined)
}

export default util
