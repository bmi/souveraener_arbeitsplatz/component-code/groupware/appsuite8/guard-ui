/* All content on this website (including text, images, source
 * code and any other original works), unless otherwise noted,
 * is licensed under a Creative Commons License.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 *
 * Copyright (C) 2016-2020 OX Software GmbH
 */

import registration from '@/io.ox/guard/pgp/register'

import('$/io.ox/office/text/app/mailactions').then(function () {
  registration.registerPasswordCheck('io.ox/mail/office/text-edit-asnew', false, 'save')
  registration.registerPasswordCheck('io.ox/mail/office/spreadsheet-edit-asnew', false, 'save')
  registration.registerPasswordCheck('io.ox/mail/office/presentation-edit-asnew', false, 'save')
  registration.registerPasswordCheck('io.ox/office/presenter/actions/launchpresenter/mail')
})
