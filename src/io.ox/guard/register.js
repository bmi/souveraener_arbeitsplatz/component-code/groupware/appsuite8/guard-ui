/**
 * All content on this website (including text, images, source
 * code and any other original works), unless otherwise noted,
 * is licensed under a Creative Commons License.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 *
 * Copyright (C) 2016-2020 OX Software GmbH
 */

import $ from '$/jquery'
import _ from '$/underscore'
import ox from '$/ox'
import ext from '$/io.ox/core/extensions'
import capabilities from '$/io.ox/core/capabilities'
import { settings } from '@/io.ox/guard/settings/guardSettings'
import guardModel from '@/io.ox/guard/core/guardModel'
import { gt } from 'gettext'

settings.ensureData().then(() => {
  // Default settings if needed
  if (settings.get('advanced') === undefined) {
    settings.set('advanced', !!settings.get('defaultAdvanced'))
    settings.save()
  }
  // Login section
  sendauth('') // initial login, no password yet
})

// Load data generated at compile (build version, etc)

// Sends first login to get settings info, key data
function sendauth (pass) {
  import('@/io.ox/guard/oxguard_core').then(({ default: oxguard }) => {
    oxguard.auth(ox.user_id, pass)
      .done(function (data) {
        try {
          /*
                import('@/io.ox/guard/oxguard_data').then((generatedData) => {
                    _.extend(data, generatedData);
                    guardModel().loadData(data);
                });
                */
          guardModel().loadData(data)
          if (data.auth.length > 20 && !(/^Error:/i.test(data.auth))) { // If good code comes back, then store userid
            guardModel().setauth(data.auth)
          } else { // If bad code, then log and mark oxguard.user_id as -1
            if (/^((Error)|(Bad)|(Lockout))/i.test(data.auth)) {
              guardModel().clearAuth()
            } else {
              guardModel().setAuth(data.auth)
            }
            if (data.error) {
              import('$/io.ox/core/notifications').then(({ default: notify }) => {
                notify.yell('error', guardModel().getName() + '\r\n' + data.error)
              })
            }
          }
          settings.ensureData().then(() => {
            if (settings.get('defaultEncrypted') === undefined) {
              import('@/io.ox/guard/settings/pane').then(({ default: init }) => {
                init(data.settings)
              })
            }
          })

          if (ox.context_id === 0) ox.context_id = data.cid // If the cid wasn't loaded, update from backend

          if (sessionStorage !== null) {
            // If we were passed login info from login screen
            try {
              if (sessionStorage.oxguard && (sessionStorage.oxguard !== 'null')) {
                const p = sessionStorage.oxguard
                sessionStorage.oxguard = null
                sendauth(p)
              }
            } catch (e) {
              console.log('Private mode')
              return
            }
          }
        } catch (e) {
          console.log(e)
          console.debug('Unable to connect to the encryption server')
        }
      })
      .fail(function () {
        console.debug('Unable to connect to the encryption server')
      })
  })
}

function doLogout (notify) {
  const def = $.Deferred()
  guardModel().clearAuth()
  import('@/io.ox/guard/api/auth').then(({ default: authAPI }) => {
    // Destroy Guard mapping and auth-token
    authAPI.resetToken().done(function () {
      if (notify) {
        import('$/io.ox/core/notifications').then(({ default: notify }) => {
          notify.yell('success', gt('Logged out of %s', guardModel().getName()))
        })
      }
    })
  })

  return def
}

ext.point('io.ox/core/appcontrol/right/help').extend({
  id: 'guard-guided-tour',
  index: 290,
  extend: function () {
    if (_.device('small') || capabilities.has('guest')) return
    if (capabilities.has('guard-mail') || capabilities.has('guard-drive')) {
      this.append(
        this.link('GuardTour', gt('Guided tour for %s', guardModel().getName()), function (e) {
          e.preventDefault()
          import('@/io.ox/guard/tour/main').then(() => {
            import('$/io.ox/core/tk/wizard').then(({ default: Tour }) => {
              Tour.registry.run('default/oxguard')
            })
          })
        })
      )
    }
  }
})

ext.point('io.ox/core/appcontrol/right/account/signouts').extend({
  id: 'logoutOG',
  index: 50,
  extend: function () {
    this.link('logoutOG', gt('Sign out %s', guardModel().getName()), function (e) {
      e.preventDefault()
      guardModel().clearAuth()
      doLogout(true)
    })
    this.$el.on('shown.bs.dropdown', function () {
      const pass = guardModel().getAuth()
      if (pass && pass.length > 10) {
        $('[data-name="logoutOG"]').show()
      } else {
        $('[data-name="logoutOG"]').hide()
      }
    })
  }
})

ext.point('io.ox/core/stages').extend({
  id: 'first',
  index: 107,
  run: function () {
    if (_.url.hash('i') === 'guestReset') {
      _.url.hash({ i: null, m: null, guardReset: 'true', app: 'io.ox/mail', settings: 'virtual/settings/io.ox/guard' })
    }
  }
})
