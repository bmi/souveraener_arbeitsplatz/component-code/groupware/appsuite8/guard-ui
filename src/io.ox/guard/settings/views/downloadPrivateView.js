/**
 * This work is provided under the terms of the CREATIVE COMMONS PUBLIC
 * LICENSE. This work is protected by copyright and/or other applicable
 * law. Any use of the work other than as authorized under this license
 * or copyright law is prohibited.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 *
 * © 2016 OX Software GmbH, Germany. info@open-xchange.com
 *
 * @author Greg Hill <greg.hill@open-xchange.com>
 */

import $ from '$/jquery'
import Backbone from '$/backbone'
import _ from '$/underscore'
import ModalDialog from '$/io.ox/backbone/views/modal'
import yell from '$/io.ox/core/yell'
import ext from '$/io.ox/core/extensions'
import core from '@/io.ox/guard/oxguard_core'
import keysAPI from '@/io.ox/guard/api/keys'
import gt from 'gettext'
import '@/io.ox/guard/settings/style.scss'

const POINT = 'io.ox/guard/settings/downloadPrivate'
let INDEX = 0

function openModalDialog (keyid) {
  return new ModalDialog({
    async: true,
    point: POINT,
    title: gt('Download Key'),
    width: 450,
    model: keysAPI.pool.get('key').get(keyid) || new Backbone.Model({ id: keyid })
  })
    .inject({
      onClick: function (e) {
        this.download({
          keyType: $(e.target).attr('data-keytype')
        })
      },
      download: function (params) {
        params = _.extend({
          keyid: this.model.get('id'),
          keyType: 'public'
        }, params)

        // public
        if (params.keyType === 'public') return keysAPI.downloadAsFile(params)

        // private, public_private
        return core.getPassword(undefined, false).then(function (data) {
          core.verifyPassword(data.password, keyid).then(function () {
            keysAPI.downloadAsFile(params, { password: data.password })
          })
        }).done(function () {
          if (!this.disposed) this.close()
        }.bind(this)).fail(function (error) {
          if (error !== 'cancel') yell('error', error)
        })
      }
    })
    .addCloseButton()
    .open()
}

ext.point(POINT).extend(
  {
    index: INDEX += 100,
    id: 'click-listener',
    render: function () {
      this.$body.on('click', 'button[data-keytype]', this.onClick.bind(this))
    }
  },
  {
    index: INDEX += 100,
    id: 'title',
    render: function () {
      const short = this.model.get('_short')
      if (!short) return
      this.$('.modal-title').append(
        $('<span>').text(': ' + short)
      )
    }
  },
  {
    index: INDEX += 100,
    id: 'description',
    render: function () {
      this.$body.append(
        $('<p>').append(
          gt('Download your private key for use with other email programs. Do not distribute this key. It is for your use only.')
        )
      )
    }
  },
  {
    index: INDEX += 100,
    id: 'private',
    render: function () {
      this.$body.append(
        $('<button type="button" class="upload-view btn btn-primary" id="downloadPrivate" data-keytype="private">')
          .text(gt('Download PGP Private Key'))
      )
    }
  },
  {
    index: INDEX += 100,
    id: 'public-private',
    render: function () {
      this.$body.append(
        $('<button type="button" class="upload-view btn btn-primary" id="downloadPrivate" data-keytype="public_private">')
          .text(gt('Download Public and Private Key'))
      )
    }
  },
  {
    index: INDEX += 100,
    id: 'public',
    render: function () {
      this.$body.append(
        $('<button type="button" class="upload-view btn btn-primary" id="downloadPrivate" data-keytype="public">')
          .text(gt('Download PGP Public Key'))
      )
    }
  }

)

export default {
  open: openModalDialog
}
