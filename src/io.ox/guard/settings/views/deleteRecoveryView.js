/**
 * This work is provided under the terms of the CREATIVE COMMONS PUBLIC
 * LICENSE. This work is protected by copyright and/or other applicable
 * law. Any use of the work other than as authorized under this license
 * or copyright law is prohibited.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 *
 * © 2016 OX Software GmbH, Germany. info@open-xchange.com
 *
 * @author Greg Hill <greg.hill@open-xchange.com>
 */

import $ from '$/jquery'
import ox from '$/ox'
import ModalView from '$/io.ox/backbone/views/modal'
import ext from '$/io.ox/core/extensions'
import yell from '$/io.ox/core/yell'
import og_http from '@/io.ox/guard/core/og_http'
import guardModel from '@/io.ox/guard/core/guardModel'
import core from '@/io.ox/guard/oxguard_core'
import gt from 'gettext'

const POINT = 'io.ox/guard/settings/reset'
let INDEX = 0

function open (node, baton) {
  return openModalDialog(node, baton)
}

function openModalDialog (node, baton) {
  return new ModalView({
    async: true,
    focus: 'input[name="active"]',
    point: POINT,
    title: gt('Delete Recovery'),
    width: 640
  })
    .inject({
    })
    .build(function () {
    })
    .addCancelButton()
    .addButton({ label: gt('Delete'), action: 'delete' })
    .on('delete', function () {
      this.close()
      performDelete().done(function () {
        ext.point('io.ox/guard/settings/detail').invoke('draw', node, baton) // redraw settings
      }).fail(this.idle).fail(yell)
    })
    .open()
}

ext.point(POINT).extend(
  {
    index: INDEX += 100,
    id: 'switch',
    render: function () {
      this.$body.append(
        $('<p>').append(gt('The password recovery is used to reset your password if you forget it.  If you delete the recovery, you will not be able to reset your password.')),
        $('<p>').append(gt('Note: Changing your password will restore the password recovery.'))
      )
    }
  }
)

function performDelete () {
  const def = $.Deferred()
  core.getPassword(gt('Please verify your password before deleting the recovery.'))
    .done(function (e) {
      const data = {
        userid: ox.user_id,
        cid: ox.context_id,
        password: e.password
      }
      core.metrics('settings', 'delete-recovery')
      og_http.post(ox.apiRoot + '/oxguard/login?action=deleterecovery', '', data)
        .done(function () {
          yell('success', gt('Recovery deleted'))
          guardModel().set('recoveryAvail', false)
          def.resolve()
        })
        .fail(function (e) {
          if (e.responseText.trim() === 'Bad password') {
            yell('error', gt('Bad Password'))
          } else {
            yell('error', e.responseText)
          }
          def.reject()
        })
    })
    .fail(function () {
      def.reject()
    })
  return def
}

export default {
  open: open
}
