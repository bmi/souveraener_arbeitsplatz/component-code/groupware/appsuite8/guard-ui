/**
 * This work is provided under the terms of the CREATIVE COMMONS PUBLIC
 * LICENSE. This work is protected by copyright and/or other applicable
 * law. Any use of the work other than as authorized under this license
 * or copyright law is prohibited.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 *
 * © 2016 OX Software GmbH, Germany. info@open-xchange.com
 *
 * @author Greg Hill <greg.hill@open-xchange.com>
 */

import $ from '$/jquery'
import ox from '$/ox'
import _ from '$/underscore'

import ModalView from '$/io.ox/backbone/views/modal'
import ext from '$/io.ox/core/extensions'
import core from '@/io.ox/guard/oxguard_core'
import authAPI from '@/io.ox/guard/api/auth'
import og_http from '@/io.ox/guard/core/og_http'
import gt from 'gettext'
import guardModel from '@/io.ox/guard/core/guardModel'
import '@/io.ox/guard/settings/style.scss'

const POINT = 'io.ox/guard/settings/reset'
let INDEX = 0

function open () {
  return openModalDialog()
}

function openModalDialog () {
  return new ModalView({
    async: true,
    focus: 'input[name="active"]',
    point: POINT,
    title: gt('Reset Password'),
    width: 640
  })
    .inject({
      doReset: function () {
        return doReset()
      }
    })
    .build(function () {

    })
    .addCancelButton()
    .addButton({ label: gt('Reset'), action: 'reset' })
    .on('reset', function () {
      this.doReset().then(this.close, this.idle)
    })
    .open()
}

ext.point(POINT).extend(
  //
  // Password Boxes
  //
  {
    index: INDEX += 100,
    id: 'switch',
    render: function () {
      this.$body.append(
        $('<p>').append(gt('This will reset your %s password, and send you a new password to the configured email address.', guardModel().getName()))
      )
    }
  }
)

async function doReset () {
  const def = $.Deferred()
  const performReset = async function (primaryEmail) {
    const { default: keyCreator } = await import('@/io.ox/guard/core/createKeys')
    const waitdiv = keyCreator.waitDiv()
    $('.modal-footer').append(waitdiv)
    waitdiv.show()
    const param = '&userid=' + ox.user_id + '&cid=' + ox.context_id +
            '&default=' + encodeURIComponent(primaryEmail) + '&lang=' + ox.language
    core.metrics('settings', 'reset-password')
    og_http.get(ox.apiRoot + '/oxguard/login?action=reset', param)
      .done(function (resp) {
        $('.og_wait').remove()
        authAPI.resetToken()
        // This is for backwards compat with 7.10.6.  Can be removed when 3.0 UI no longer used with old backend
        let data = {}
        if (_.isString(resp)) {
          data.result = resp
        } else {
          data = resp
        }
        //  End of temp fix.
        if (data.error) {
          core.notifyError(gt('Unable to reset your password') + ': ' + data.error)
          def.reject()
          return
        }
        if (data.result === 'primary') {
          core.notify('success', gt('A new password has been sent to your email address.'))
          guardModel().setAuth('Password Needed')
          def.resolve()
          return
        }
        if (data.result === 'ok') {
          core.notify('success', gt('A new password has been sent to your secondary email address.'))
          guardModel().setAuth('Password Needed')
          def.resolve()
          return
        }
        if (data.result === 'NoRecovery') {
          core.notifyError(gt('No password recovery is available.'))
          def.reject()
          return
        }
        if (data.result === 'FailNotify') {
          core.notifyError(gt('Password reset, but unable to send to your email address.'))
          def.reject()
          return
        }
        if (data.result === 'NoSecondary') {
          core.notifyError(gt('Unable to find email address to send password reset.'))
          def.reject()
          return
        }
        core.notifyError(gt('Unable to reset your password'))
        def.reject()
      })
      .fail(function () {
        $('.og_wait').remove()
        core.notifyError(gt('Unable to reset your password'))
        def.reject()
      })
  }

  import('$/io.ox/core/api/account').then(({ default: api }) => {
    api.getPrimaryAddress().done(function (data) { // Get primary address as backup
      const primaryEmail = data[1]
      performReset(primaryEmail)
    })
      .fail(function () {
        performReset(ox.user) // Possible guest, no primary email
      })
  })
  return def
}

export default {
  open: open
}
