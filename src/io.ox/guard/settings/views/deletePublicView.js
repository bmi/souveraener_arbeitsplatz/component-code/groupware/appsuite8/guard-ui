/**
 * This work is provided under the terms of the CREATIVE COMMONS PUBLIC
 * LICENSE. This work is protected by copyright and/or other applicable
 * law. Any use of the work other than as authorized under this license
 * or copyright law is prohibited.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 *
 * © 2016 OX Software GmbH, Germany. info@open-xchange.com
 *
 * @author Greg Hill <greg.hill@open-xchange.com>
 */

import $ from '$/jquery'
import ModalDialog from '$/io.ox/backbone/views/modal'
import yell from '$/io.ox/core/yell'
import '@/io.ox/guard/oxguard_core'
import keysAPI from '@/io.ox/guard/api/keys'
import gt from 'gettext'

function openModalDialog (keyId) {
  return new ModalDialog({
    async: true,
    point: 'io.ox/guard/settings/deletePublic',
    title: gt('Delete Public Key'),
    width: 400,
    enter: 'cancel'
  })
    .build(function () {
      this.$body.append(
        $('<p>').text(gt('Please verify you want to delete this key'))
      )
    })
    .addButton({ label: gt('Delete'), action: 'delete', className: 'btn-default' })
    .addCloseButton()
    .on('delete', function () {
      keysAPI.delete(keyId).done(function () {
        yell('success', gt('Key Deleted'))
        this.close()
      }.bind(this))
    })
    .open()
}

export default {
  open: openModalDialog
}
