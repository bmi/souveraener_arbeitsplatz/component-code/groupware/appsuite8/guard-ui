/**
 * This work is provided under the terms of the CREATIVE COMMONS PUBLIC
 * LICENSE. This work is protected by copyright and/or other applicable
 * law. Any use of the work other than as authorized under this license
 * or copyright law is prohibited.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 *
 * © 2016 OX Software GmbH, Germany. info@open-xchange.com
 *
 * @author Greg Hill <greg.hill@open-xchange.com>
 */

import ModalDialog from '$/io.ox/backbone/views/modal'
import userKeys from '@/io.ox/guard/settings//userKeys'
import gt from 'gettext'

const POINT = 'io.ox/guard/settings/userkeys'

function openModalDialog () {
  return new ModalDialog({
    async: true,
    point: POINT,
    title: gt('Your Keys'),
    id: 'userKeyList',
    width: 640
  })
    .extend({
      body: function () {
        this.$el.addClass('maxheight')
        this.$body.append(
          userKeys.userKeys()
        )
      }
    })
    .addCloseButton()
    .open()
}

export default {
  open: openModalDialog
}
