/**
 * This work is provided under the terms of the CREATIVE COMMONS PUBLIC
 * LICENSE. This work is protected by copyright and/or other applicable
 * law. Any use of the work other than as authorized under this license
 * or copyright law is prohibited.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 *
 * © 2016 OX Software GmbH, Germany. info@open-xchange.com
 *
 * @author Greg Hill <greg.hill@open-xchange.com>
 */

import $ from '$/jquery'
import ModalDialog from '$/io.ox/backbone/views/modal'
import gt from 'gettext'
import pubkeys from '@/io.ox/guard/pgp/keyDetails'

function openModalDialog () {
  return new ModalDialog({
    async: true,
    point: 'io.ox/guard/settings/autoCryptkeys',
    title: gt('Autocrypt Keys'),
    id: 'autoCryptList',
    width: 560
  })
    .extend({
      header: function () {
        this.$header.append(
          $('<div class="autocryptHeader">').append(
            $('<span>').append(
              gt('These keys have been collected from email headers.  Keys are used for encryption, but not signatures verification until the key is verified by you.')
            )
          ))
      },
      'list-public': function () {
        this.$el.addClass('maxheight')
        this.$body.append(
          pubkeys.listPublic({
            id: 'autoCrypt',
            title: gt('Collected keys'),
            minimal: true
          })
        )
      }
    })
    .addCloseButton()
    .open()
}

export default {
  open: openModalDialog
}
