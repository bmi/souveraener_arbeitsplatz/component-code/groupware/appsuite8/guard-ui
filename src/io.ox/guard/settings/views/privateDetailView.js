/**
 * This work is provided under the terms of the CREATIVE COMMONS PUBLIC
 * LICENSE. This work is protected by copyright and/or other applicable
 * law. Any use of the work other than as authorized under this license
 * or copyright law is prohibited.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 *
 * © 2016 OX Software GmbH, Germany. info@open-xchange.com
 *
 * @author Greg Hill <greg.hill@open-xchange.com>
 */

import $ from '$/jquery'
import moment from '$/moment'
import _ from '$/underscore'
import Backbone from '$/backbone'
import keysAPI from '@/io.ox/guard/api/keys'
import ModalView from '$/io.ox/backbone/views/modal'
import icons from '@/io.ox/guard/core/icons'
import '@/io.ox/guard/settings//style.scss'
import '@/io.ox/guard/pgp/style.scss'
import gt from 'gettext'

function openModalDialog (data) {
  return new ModalView({
    async: true,
    point: 'io.ox/guard/settings/privateDetailView',
    title: gt('Key Details'),
    id: 'privKeyDetail',
    width: 640,
    model: keysAPI.pool.get('key').get(data.id) || new Backbone.Model(data)
  })
    .inject({
      showSignatures: function () {
        // Pull list of signatures
        const keyId = this.model.get('id')
        import('@/io.ox/guard/pgp/signatureView').then(({ default: view }) => {
          view.open(keyId)
        })
      },
      getInfo: function (opt) {
        return $('<div class="info-line stripes-red">').append(
          opt.icon ? icons.getIcon({ name: opt.icon }) : $(),
          $('<span class="text">').append(
            $.txt(opt.text)
          )
        )
      }
    })
    .extend({
      classnames: function () {
        this.$body
        // allow mouse selection
          .addClass('selectable-text')
        // add flags (classnames)
          .addClass(this.model.get('revoked') ? 'revoked' : '')
          .addClass(this.model.get('expired') ? 'expired' : '')
      },
      'info-line': function () {
        this.$body.append(
          $('<section class="info">').append(
            this.model.get('revoked') ? this.getInfo({ text: gt('Revoked') }) : $()
          )
        )
      },
      detail: function () {
        const model = this.model
        const expires = model.get('_expires') ? new moment(model.get('_expires')) : undefined
        this.$body.append(
          $('<section class="details">')
            .append(
              $('<dl>').append(
                // short id
                $('<dt>').text(gt('ID')),
                $('<dd>').text(this.model.get('_short')),
                // full fingerprint
                $('<dt>').text(gt('Fingerprint')),
                $('<dd>').text(this.model.get('fingerPrint')),
                // expires
                $('<dt data-dt="expires">').text(data.expired ? gt('Expired') : gt('Expires')),
                $('<dd data-dd="expires">').text(expires ? expires.format('ll') + ' (' + expires.fromNow() + ')' : '-----'),
                // created
                $('<dt>').text(gt('Created')),
                $('<dd>').text(new moment(this.model.get('creationTime')).format('ll')),
                // has private key
                $('<dt>').text(gt('Private Key')),
                $('<dd>').text(this.model.get('hasPrivateKey') ? gt('yes') : gt('no')),
                // is masterkey
                $('<dt>').text(gt('Master')),
                $('<dd>').text(this.model.get('masterKey') ? gt('yes') : gt('no')),
                // used for encryption
                $('<dt>').text(gt('Encryption')),
                $('<dd>').text(this.model.get('encryptionKey') ? gt('yes') : gt('no')),
                // ids
                $('<dt>').text(gt('IDs')),
                _.map(this.model.get('userIds'), function (user) {
                  return $('<dd>').text(user)
                })
              )
            )
        )
      }
    })
    .addAlternativeButton({ label: gt('Signatures'), action: 'signatures' })
    .addCloseButton()
    .on('signatures', function () {
      this.showSignatures()
      this.idle()
    })
    .open()
}

export default {
  open: openModalDialog
}
