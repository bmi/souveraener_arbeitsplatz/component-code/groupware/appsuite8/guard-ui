/**
 * This work is provided under the terms of the CREATIVE COMMONS PUBLIC
 * LICENSE. This work is protected by copyright and/or other applicable
 * law. Any use of the work other than as authorized under this license
 * or copyright law is prohibited.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 *
 * © 2016 OX Software GmbH, Germany. info@open-xchange.com
 *
 * @author Greg Hill <greg.hill@open-xchange.com>
 */

import pubkeys from '@/io.ox/guard/pgp/keyDetails'
import _ from '$/underscore'
import ModalDialog from '$/io.ox/backbone/views/modal'

import gt from 'gettext'

function openModalDialog (options) {
  return new ModalDialog(_.extend({
    async: false,
    point: 'io.ox/guard/settings/pubkeys',
    title: gt('Public Keys'),
    id: 'pkeyList',
    width: '75%'
  }, options))
    .extend({
      'list-public': function () {
        this.$el.addClass('maxheight')
        this.$body.append(
          pubkeys.listPublic({
            id: 'settings'
          })
        )
      }
    })
    .addCloseButton()
    .open()
}

export default {
  open: openModalDialog
}
