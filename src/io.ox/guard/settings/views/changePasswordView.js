/**
 * This work is provided under the terms of the CREATIVE COMMONS PUBLIC
 * LICENSE. This work is protected by copyright and/or other applicable
 * law. Any use of the work other than as authorized under this license
 * or copyright law is prohibited.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 *
 * © 2016 OX Software GmbH, Germany. info@open-xchange.com
 *
 * @author Greg Hill <greg.hill@open-xchange.com>
 */

import $ from '$/jquery'
import _ from '$/underscore'
import ox from '$/ox'
import ModalView from '$/io.ox/backbone/views/modal'
import ext from '$/io.ox/core/extensions'
import core from '@/io.ox/guard/oxguard_core'
import guardModel from '@/io.ox/guard/core/guardModel'
import PasswordView from '@/io.ox/guard/core/passwordView'
import authAPI from '@/io.ox/guard/api/auth'
import loginAPI from '@/io.ox/guard/api/login'
import '@/io.ox/guard/settings/style.scss'

import gt from 'gettext'

const POINT = 'io.ox/guard/settings/passwords'
let INDEX = 0

function open (node, baton) {
  return openModalDialog(node, baton)
}

function openModalDialog (node, baton) {
  return new ModalView({
    async: true,
    focus: 'input[name="active"]',
    point: POINT,
    title: gt('Change Password'),
    width: 640
  })
    .inject({
      doChange: function () {
        const newpass1 = $('#newpass1')
        const newpass2 = $('#newpass2')
        const oldpass = $('#oldpass')
        return changePass(newpass1, newpass2, oldpass)
      }
    })
    .addCancelButton()
    .addButton({ label: gt('Change'), action: 'change' })
    .on('change', function () {
      const view = this
      this.doChange().then(function () {
        view.close()
        ext.point('io.ox/guard/settings/detail').invoke('draw', node, baton) // redraw settings
      }, function () {
        view.idle()
      })
    })
    .open()
}

ext.point(POINT).extend(
  //
  // Password Boxes
  //
  {
    index: INDEX += 100,
    id: 'switch',
    render: function () {
      this.$body.append(
        generatePasswordPrompts()
      )
    }
  }
)

function generatePasswordPrompts () {
  const passheader =
            $('<div class="oxguard_settings password"/>')
  const oldpass = new PasswordView.view({ id: 'oldpass', class: 'password_prompt' }).getInputField()
  const newpass1 = new PasswordView.view({ id: 'newpass1', class: 'password_prompt', validate: true })
  const newpass2 = new PasswordView.view({ id: 'newpass2', class: 'password_prompt' })
  const td = $('<td>').append(newpass1.getInputField())
  const td2 = $('<td>').append(newpass2.getInputField())
  const hint = $('<td style="width:150px;">')
  const curPrompt = $('<label for="oldpass">').append(gt('Enter current %s security password', guardModel().getName()))
  const prompt1 = $('<label for="newpass1">').append(gt('Enter new %s security password', guardModel().getName()))
  const prompt2 = $('<label for="newpass2">').append(gt('Verify new %s security password', guardModel().getName()))
  let passwordTable
  if (_.device('small')) {
    passwordTable = $('<div style="padding:10px;">')
    if (!guardModel().get('new')) {
      passwordTable // #. %s product name
        .append(curPrompt).append('<br>').append(oldpass).append('<br>')
    }
    passwordTable
      .append(prompt1).append('<br>').append(newpass1.getProtected()).append('<br>')
      .append(prompt2).append('<br>').append(newpass2.getProtected)
  } else {
    passwordTable = $('<table/>')
    if (!guardModel().get('new')) {
      passwordTable // #. %s product name
        .append($('<tr>').append(($('<td>').append(curPrompt))).append($('<td>').append(oldpass)))
    }
    passwordTable
      .append($('<tr>').append(($('<td>').append(prompt1))).append(td).append(hint))
      .append($('<tr>').append(($('<td>').append(prompt2))).append(td2).append($('<td>')))
  }

  if (guardModel().get('pin')) {
    const pinPrompt = $('<p>').append(gt('The sender assigned an additional PIN to the account.  This PIN must be entered before you can use this account.  Please enter it now.  Contact the sender directly if you don\'t yet have a pin.'))
    const pinTable = $('<table class="og_password_prompt">')
    const pinLabel = $('<td class="pw">').append('<label for="pinPrompt">' + gt('PIN') + ':</label>')
    const pinEntry = $('<input id="pinInput" name="pinInput" class="form-control password_prompt">')
    passheader.append(pinPrompt).append(pinTable.append($('<tr>').append(pinLabel).append($('<td>').append(pinEntry)))).append('<hr/>')
  }
  import('@/io.ox/guard/core/passwords').then(({ default: pass }) => {
    pass.passwordCheck(newpass1, hint)
    pass.passwordCompare(newpass2, newpass1, hint)
  })
  return passheader.append(passwordTable)
}

async function changePass (newpass, newpass2, oldpass) {
  const def = $.Deferred()
  const min = guardModel().getSettings().min_password_length
  if (min !== undefined) {
    if (newpass.val().length < min) {
      showError(gt('New password must be at least %s characters long', min))
      if (!_.device('ios')) {
        newpass.focus()
      }
      def.reject()
      return def
    }
  }
  if (newpass.val() !== newpass2.val()) {
    newpass2.css('background-color', 'salmon')
    def.reject()
    return def
  }
  newpass2.css('background-color', 'white')
  const userdata = {
    newpass: newpass.val(),
    oldpass: oldpass.val(),
    sessionID: ox.session,
    cid: ox.context_id,
    userEmail: ox.user,
    pin: guardModel().get('ping') ? $('#pinInput').val() : ''
  }
  const { default: keyCreator } = await import('@/io.ox/guard/core/createKeys')
  const waitdiv = keyCreator.waitDiv()
  $('.modal-footer').append(waitdiv)
  waitdiv.show()
  loginAPI.changePassword(userdata)
    .done(function (data) {
      $('.og_wait').remove()
      if (typeof data === 'string') data = $.parseJSON(data)
      if (data.auth === undefined) {
        import('@/io.ox/guard/core/errorHandler').then(({ default: err }) => {
          err.showError(data)
        })
        def.reject()
        return
      }
      if (data.auth === 'Bad password') {
        showError(gt('Bad password'))
      }
      if (data.auth === 'Lockout') {
        showError(gt('Temporary Lockout'))
      }
      if (data.auth === 'Bad new password') {
        showError(gt('New password must be at least %s characters long', data.minlength))
      }
      if (data.auth.length > 20) {
        $('input[name="newpass1"]').val('')
        $('input[name="newpass2"]').val('')
        $('input[name="oldpass"]').val('')
        guardModel().clearAuth()
        guardModel().set('pin', false)
        guardModel().set('recoveryAvail', data.recovery)
        guardModel().set('new', false)
        showOK(gt('Password changed successfully'))
        // We want to remove remembered auth codes, as the password has changed
        authAPI.resetToken().always(function () {
          def.resolve()
        })
      }
    })
    .fail(function (e) {
      $('.og_wait').remove()
      let error = gt('Failed to change password')
      if (e.code === 'GRD-AUTH-0002') {
        error = gt('Bad password')
      }
      showError(error)
      def.reject()
    })
  return def
}

function showOK (msg) {
  core.notify('success', msg)
}

function showError (msg) {
  core.notifyError(msg)
}

export default {
  open: open
}
