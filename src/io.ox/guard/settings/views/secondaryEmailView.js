/**
 * This work is provided under the terms of the CREATIVE COMMONS PUBLIC
 * LICENSE. This work is protected by copyright and/or other applicable
 * law. Any use of the work other than as authorized under this license
 * or copyright law is prohibited.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 *
 * © 2016 OX Software GmbH, Germany. info@open-xchange.com
 *
 * @author Greg Hill <greg.hill@open-xchange.com>
 */

import $ from '$/jquery'
import _ from '$/underscore'
import ox from '$/ox'
import ModalView from '$/io.ox/backbone/views/modal'
import og_http from '@/io.ox/guard/core/og_http'
import PasswordView from '@/io.ox/guard/core/passwordView'
import verify from '@/io.ox/guard/core/emailverify'
import guardModel from '@/io.ox/guard/core/guardModel'
import yell from '$/io.ox/core/yell'
import ext from '$/io.ox/core/extensions'
import core from '@/io.ox/guard/oxguard_core'
import gt from 'gettext'

const POINT = 'io.ox/guard/settings/secondary'
let INDEX = 0

function open () {
  return getSecondary().then(openModalDialog, fail)
}

function fail (e) {
  yell('error', e.code === 'GUARD_FAILUER'
    ? gt('Unable to retrieve email address for password reset')
    : gt('Please retry later.')
  )
  throw e
}

function openModalDialog (secondary) {
  return new ModalView({
    async: true,
    focus: 'input[name="active"]',
    point: POINT,
    title: gt('Secondary Email Address'),
    width: 640
  })
    .inject({
      doChange: function () {
        const email = $('input[name="newemail"]')
        const verify = $('input[name="newemailverify"]')
        const password = $('#ogpass')
        return changeSecondary(email, verify, password)
      },
      updateEmail: function () {
        this.$body.find('#secondaryEmail').text(secondary)
      }
    })
    .build(function () {

    })
    .addCancelButton()
    .addButton({ label: gt('Change email address'), action: 'change' })
    .on('change', function () {
      this.doChange().done(this.close).fail(this.idle).fail(yell)
    })
    .on('open', function () {
      this.updateEmail()
    })
    .open()
}

ext.point(POINT).extend(
  {
    index: INDEX += 100,
    id: 'switch',
    render: function () {
      this.$body.append(
        createSecondaryChange()
      )
    }
  }
)

function createSecondaryChange () {
  if (guardModel().getSettings().noRecovery === true || guardModel().get('recoveryAvail') === false) return // Don't display secondary email if no recovery is set
  const current = $('<span>').append(gt('Current email address:'))
  const currentEmail = $('<span id="secondaryEmail" style="padding-left:20px;">')
  const emailheader =
            $('<div class="oxguard_settings"/>')
              .append('<span> ' + gt('Change email address used for password reset.') + '</span><br/><br/>')
  if (_.device('small')) {
    emailheader.append(current).append('<br>').append(currentEmail)
  } else {
    emailheader.append(current).append(currentEmail)
  }
  const noSaveWorkAround = $('<input style="display:none" type="text" name="dontremember"/><input style="display:none" type="password" name="dontrememberpass"/>')
  const emailAddr = $('<input name="newemail" id="newemail" class="form-control"/>')
  const verifyPass = $('<input name="newemailverify" id="newemailverify" class="form-control">')
  const currentPrompt = $('<label for="ogpass">').append(gt('Enter current %s security password', guardModel().getName()))
  const currentInput = new PasswordView.view({ id: 'ogpass', class: 'password_prompt' }).getProtected()
  const newPrompt = $('<label for="newemail">').append(gt('Enter new secondary email address'))
  const verifyPrompt = $('<label for="newemailverify">').append(gt('Verify email address'))
  const hint = $('<td>')
  let emailTable
  if (_.device('small')) {
    emailTable = $('<div style="margin-top:15px;margin-left:10px;">')
      .append(currentPrompt).append('<br>').append(currentInput).append('<br>')
      .append(newPrompt).append('<br>').append(emailAddr).append('<br>')
      .append(verifyPrompt).append('<br>').append(verifyPass)
  } else {
    emailTable = $('<table/>')
      .append(noSaveWorkAround)
    // #. %s product name
      .append($('<tr>').append($('<td>').append(currentPrompt)).append($('<td>').append(currentInput)).append($('<td>')))
      .append($('<tr>').append($('<td>').append(newPrompt)).append($('<td>').append(emailAddr)).append(hint))
      .append($('<tr>').append($('<td>').append(verifyPrompt)).append($('<td>').append(verifyPass)).append($('<td>')))
  }
  const errorDiv = $('<div id="newemailerror" class="alert alert-info" style="display:none"/>')
  verify.setValidate(emailAddr, hint)
  verify.autoCompare(emailAddr, verifyPass, hint)
  return (emailheader.append(emailTable).append(errorDiv))
}

function getSecondary () {
  const def = $.Deferred()
  og_http.get(ox.apiRoot + '/oxguard/login?action=secondary', '&userid=' + ox.user_id + '&cid=' + ox.context_id)
    .done(function (d) {
      d = d.trim()
      if (d.length < 2) {
        import('$/io.ox/core/api/account').then(({ default: accountAPI }) => {
          accountAPI.getPrimaryAddress().then(function (addr) {
            if (addr.length < 2) {
              def.reject()
            } else {
              def.resolve(addr[1])
            }
          })
        })
      } else {
        def.resolve(d)
      }
    })
    .fail(function () {
      def.reject()
    })
  return def
}

function changeSecondary (emailInput, verifyEmailInput, passwordInput) {
  const def = $.Deferred()
  const email = emailInput.val()
  const verifyEmail = verifyEmailInput.val()
  if (email !== verifyEmail) {
    showError(gt('Emails not equal'))
    def.reject()
    return def
  }
  if (!verify.validate(email)) {
    showError(gt('Invalid email address'))
    def.reject()
    return def
  }
  const data = {
    userid: ox.user_id,
    cid: ox.context_id,
    email: email,
    password: passwordInput.val()
  }
  core.metrics('settings', 'change-secondary-email')
  og_http.post(ox.apiRoot + '/oxguard/login?action=changesecondary', '', data)
    .done(function () {
      showOK(gt('Successfully changed email address'))
      $('input[name="ogpass"]').val('')
      $('input[name="newemail"]').val('')
      $('input[name="newemailverify"]').val('')
      def.resolve()
    })
    .fail(function (d) {
      if (d.responseText.trim() === 'Bad password') {
        showError(gt('Bad Password'))
        if (!_.device('ios')) {
          passwordInput.focus()
        }
      }
      def.reject()
    })

  return def
}

function showOK (msg) {
  core.notify('success', msg)
}

function showError (msg) {
  core.notifyError(msg)
}

export default {
  open: open
}
