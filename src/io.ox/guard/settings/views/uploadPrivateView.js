/**
 * This work is provided under the terms of the CREATIVE COMMONS PUBLIC
 * LICENSE. This work is protected by copyright and/or other applicable
 * law. Any use of the work other than as authorized under this license
 * or copyright law is prohibited.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 *
 * © 2016 OX Software GmbH, Germany. info@open-xchange.com
 *
 * @author Greg Hill <greg.hill@open-xchange.com>
 */

import $ from '$/jquery'
import ModalView from '$/io.ox/backbone/views/modal'
import guardModel from '@/io.ox/guard/core/guardModel'
import yell from '$/io.ox/core/yell'
import ext from '$/io.ox/core/extensions'
import '@/io.ox/guard/oxguard_core'
import gt from 'gettext'
import '@/io.ox/guard/settings/style.scss'

const POINT = 'io.ox/guard/settings/uploadPrivate'
let INDEX = 0

let dialog

function open () {
  dialog = openModalDialog()
  return dialog
}

function openModalDialog () {
  return new ModalView({
    async: true,
    point: POINT,
    title: gt('Adding Keys'),
    width: 640
  })
    .inject({
    })
    .build(function () {
    })
    .addCancelButton()
    .open()
}

ext.point(POINT).extend(
  {
    index: INDEX += 100,
    id: 'uploadPrivate',
    render: function () {
      const label = $('<label for="uploadPrivateButton">').append(gt('If you already have a private key, you can upload it here.'))
        .append('<br>')
      const button = $('<button type="button" class="upload-view btn btn-primary" id="uploadPrivateButton">')
        .text(gt('Upload private key'))
        .on('click', function () {
          openUploadPrivateDialog(true)
        })
      this.$body.append(
        label.append(button)
      )
    }
  },
  {
    index: INDEX += 100,
    id: 'uploadPublic',
    render: function () {
      const label = $('<label for="uploadPublicButton">').append(
        gt('If you would like to upload a public key only, then do so here.  This will allow you to encrypt files and receive encrypted emails using %s, but you will need to use the private key with another program to decode the items.', guardModel().getName()))
        .append('<br>')
      const button = $('<button type="button" class="upload-view btn btn-primary" id="uploadPublicButton">')
        .text(gt('Upload public key only'))
        .on('click', function () {
          openUploadPrivateDialog(false)
        })
      this.$body.append(
        label.append(button)
      )
    }
  },
  {
    index: INDEX += 100,
    id: 'createNewKeys',
    render: function () {
      const label = $('<label for="createNewButton">').append(gt('Finally, you can have a new PGP Key Pair created for your account.'))
        .append('<br>')
      const dialog = this
      const button = $('<button type="button" class="upload-view btn btn-primary" id="createNewButton">')
        .text(gt('Create new keys'))
        .on('click', function () {
          createNew()
          dialog.close()
        })
      this.$body.append(
        label.append(button)
      )
    }
  },
  {
    index: INDEX += 100,
    id: 'fileInputArea',
    render: function () { // file input area, hidden until button pressed
      this.$body.append($('<input type="file" id="privateKeyFileInput" style="display:none;">'))
    }
  }

)

// Opens the file input and prepares for uploading key
// priv is true if includes private key (will require password prompt, etc)
function openUploadPrivateDialog (priv) {
  const fileinput = $('#privateKeyFileInput')
  fileinput.unbind('change')
  fileinput.on('change', function () {
    const files = this.files
    if (files.length > 0) {
      import('@/io.ox/guard/pgp/uploadkeys').then(({ default: uploader }) => {
        if (priv) {
          uploader.uploadPrivate(files)
            .done(function () {
              dialog.close()
              guardModel().clearAuth()
              $('#refreshuserkeys').click()
            })
            .fail(function () {
              dialog.idle()
            })
            .always(function () {
              fileinput.val('')
            })
        } else {
          uploader.upload(files)
            .done(function () {
              dialog.close()
              $('#refreshuserkeys').click()
            })
            .fail(function () {
              dialog.idle()
            })
            .always(function () {
              fileinput.val('')
            })
        }
      })
    }
  })
  fileinput.click()
}

function createNew () {
  import('@/io.ox/guard/core/createKeys').then(({ default: keys }) => {
    keys.createKeys()
      .done(function () {
        $('#refreshuserkeys').click()
      })
      .fail(function (e) {
        if (e === 'cancel') {
          return
        }
        yell('error', e.responseText)
      })
  })
}

export default {
  open: open
}
