/**
 * This work is provided under the terms of the CREATIVE COMMONS PUBLIC
 * LICENSE. This work is protected by copyright and/or other applicable
 * law. Any use of the work other than as authorized under this license
 * or copyright law is prohibited.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 *
 * © 2016 OX Software GmbH, Germany. info@open-xchange.com
 *
 * @author Greg Hill <greg.hill@open-xchange.com>
 */

import $ from '$/jquery'
import ox from '$/ox'
import Backbone from '$/backbone'
import { input as utilInput } from '$/io.ox/core/settings/util'
import http from '@/io.ox/guard/core/og_http'
import ModalView from '$/io.ox/backbone/views/modal'
import yell from '$/io.ox/core/yell'
import ext from '$/io.ox/core/extensions'
import core from '@/io.ox/guard/oxguard_core'
import util from '@/io.ox/guard/util'
import PasswordView from '@/io.ox/guard/core/passwordView'
import gt from 'gettext'

const POINT = 'io.ox/guard/settings/editUserIdView'
let INDEX = 0

let dialog

function open (id) {
  dialog = openModalDialog(id)
  return dialog
}

function openModalDialog (id) {
  return new ModalView({
    async: true,
    point: POINT,
    title: gt('Adding UserID'),
    width: 450
  })
    .inject({
    })
    .build(function () {
    })
    .addCancelButton()
    .addButton({ label: gt('Add'), action: 'add' })
    .on('add', function () {
      const dialog = this
      editUserId(id)
        .done(function () {
          yell('success', gt('User ID added'))
          dialog.close()
          $('#refreshuserkeys').click()
        })
        .fail(function (e) {
          if (e && e.length > 1) yell('error', e)
          dialog.idle()
        })
    })
    .open()
}

ext.point(POINT).extend(
  {
    index: INDEX += 100,
    id: 'header',
    render: function () {
      this.$body.addClass('addUserId')
      const label = $('<label>').append(gt('Add a name and email address to this public key.'))
        .append('<br>')
      this.$body.append(
        $('<div>').append(label)
      )
    }
  },
  {
    index: INDEX += 100,
    id: 'name',
    render: function () {
      const input = utilInput('nameInput', gt('Name'), new Backbone.Model())
      this.$body.append(
        input
      )
    }
  },
  {
    index: INDEX += 100,
    id: 'email',
    render: function () {
      const input = utilInput('emailInput', gt('Email'), new Backbone.Model())
      this.$body.append(
        input
      )
    }
  },
  {
    index: INDEX += 100,
    id: 'password',
    render: function () {
      const label = $('<label for="passwordInput" id="passwordLabel">').append(gt('Please enter your key password'))
        .append('<br>')
      const input = new PasswordView.view({ id: 'passwordInput' }).getProtected()
      this.$body.append(label).append(input)
    }
  }
)

function editUserId (id) {
  const def = $.Deferred()
  const email = $('[name="emailInput"]').val()
  const name = $('[name="nameInput"]').val()
  const password = $('#passwordInput').val()
  if (!util.validateEmail(email)) {
    def.reject(gt('Invalid Email Address'))
    return def
  }
  if (name.length < 2) {
    def.reject(gt('Invalid Name'))
    return def
  }
  if (password.length < 2) {
    def.reject(gt('Enter password'))
    return def
  }
  const data = {
    password: password,
    email: email,
    name: name,
    keyid: id
  }
  http.simplePost(ox.apiRoot + '/oxguard/keys?action=addUserId', '', data)
    .done(function (data) {
      if (core.checkJsonOK(data)) {
        def.resolve(gt('User ID added'))
      } else {
        def.reject()
      }
    })
    .fail(function (e) {
      def.reject(gt('Failed to add UserID') + e.responseText)
    })
  return def
}

export default {
  open: open
}
