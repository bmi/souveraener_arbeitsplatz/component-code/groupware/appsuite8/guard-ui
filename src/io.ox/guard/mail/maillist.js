/* All content on this website (including text, images, source
 * code and any other original works), unless otherwise noted,
 * is licensed under a Creative Commons License.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 *
 * Copyright (C) 2016-2020 OX Software GmbH
 */

import extensions from '$/io.ox/mail/common-extensions'
import mailApi from '$/io.ox/mail/api'
import ext from '$/io.ox/core/extensions'
import icons from '@/io.ox/guard/core/icons'
import '@/io.ox/guard/mail/style.scss'

// Override default icon extension for encrypted icons
extensions.pgp.encrypted = function (baton) {
  if (!/^multipart\/encrypted/.test(baton.data.content_type) &&
                !(baton.model.get('security_info') && baton.model.get('security_info').encrypted) &&
                !(baton.model.get('security') && baton.model.get('security').decrypted)) return
  baton.data.text_preview = '' // wipe the preview
  this.append(
    // icons.getIcon({ name: 'lock-fill', className: 'guard_maillist encrypted' })
    icons.getLocked('guard_maillist encrypted')
  )
}

extensions.pgp.signed = function (baton) {
  // simple check for signed mail
  if (!/^multipart\/signed/.test(baton.data.content_type) &&
            !(baton.data.security && baton.data.security.signatures)) return

  this.append(
    icons.getIcon({ name: 'pencil-square', className: 'guard_maillist signed' })
  )
}

// Disable default paper clip, as all oxguard emails have attachments.  Will call manually.
ext.point('io.ox/mail/listview/item/default/row2').disable('paper-clip')
ext.point('io.ox/mail/listview/item/small/col4').disable('paper-clip')

ext.point('io.ox/mail/listview/item').extend({
  id: 'checkReload',
  index: 10,
  draw: function (baton) {
    const pooledMail = mailApi.pool.get('detail').get(baton.data.cid)
    if (pooledMail) {
      if (pooledMail.get('guardIcons')) { // PGP emails may have been already decrypted, etc
        baton.data.security = pooledMail.get('guardIcons').security
        baton.data.attachment = pooledMail.get('guardIcons').attachment
      }
    }
  }
})

ext.point('io.ox/mail/listview/item/default/row2').extend({
  id: 'OGpaper-clip',
  index: 300,
  draw: function (baton) {
    addLock.call(this, baton)
  }
})

ext.point('io.ox/mail/listview/item/small/col5').disable('paper-clip')
ext.point('io.ox/mail/listview/item/small/col5').extend({
  id: 'OGpaper-clipSm',
  index: 300,
  draw: function (baton) {
    addLock.call(this, baton)
  }
})

// Check if OxGuard email, and if so, add lock icon.  Add paperclip if applicable
function addLock (baton) {
  try {
    if (baton.data.attachment === undefined) {
      return
    }
    if (baton.model === undefined) {
      extensions.paperClip.call(this, baton)
      return
    }
    // Encrypted, we don't know if attachments
    if (baton.model.get('security_info') && baton.model.get('security_info').encrypted) {
      if (!(baton.model.get('security') && baton.model.get('security').decrypted)) {
        return
      }
    }
    /// PGP Components
    // Check if decoded
    if (baton.model.get('security') && baton.model.get('security').decrypted) {
      if (baton.model.get('attachment') === true) {
        extensions.paperClip.call(this, baton)
      }
      return
    }
    if (/multipart\/encrypted/.test(baton.model.get('content_type'))) {
      return
    }
    extensions.paperClip.call(this, baton) // Otherwise, call paperclip
  } catch (ex) {
    console.log('Problem checking list for OG email')
    console.log(ex)
  }
}
