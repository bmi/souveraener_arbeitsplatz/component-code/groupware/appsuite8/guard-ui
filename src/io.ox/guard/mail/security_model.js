/**
 * All content on this website (including text, images, source
 * code and any other original works), unless otherwise noted,
 * is licensed under a Creative Commons License.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 *
 * Copyright (C) 2016-2020 OX Software GmbH
 */

import Backbone from '$/backbone'
import _ from '$/underscore'

const SecurityModel = Backbone.Model.extend({
  initialize: function (init, mailModel) {
    this.mailModel = mailModel
    this.set('PGPSignature', init.sign)
    this.set('encrypt', init.encrypt)
    this.set('PGPFormat', init.pgpInline ? 'inline' : 'mime')
    this.set('authToken', init.authToken)
    this.listenTo(this, 'change', this.onChange)
  },
  setMailModel: function (model) {
    this.mailModel = model
  },
  events: {
    change: 'onChange',
    set: 'onChange'
  },
  defaults: {
    encrypt: false,
    sign: false
  },
  toJson: function () {
    return _.extend(
      {}, (this.mailModel ? this.mailModel.get('security') : {}), {
        encrypt: this.get('encrypt'),
        sign: this.get('PGPSignature'),
        pgpInline: this.get('PGPFormat') === 'inline',
        authToken: this.get('authToken')
      })
  },
  onChange: function (model, silent) {
    if (this.mailModel) {
      if (this.get('encrypt') && !this.get('authToken')) {
        const mailModel = this.mailModel
        if (this.get('tour')) {
          return
        }
        import('@/io.ox/guard/auth').then(({ default: auth }) => {
          auth.authorize().then(function (authcode) {
            model.set('authToken', authcode)
            mailModel.set('security', model.toJson())
            mailModel.save()
          }, function () {
            model.set('encrypt', false)
          })
        })
      } else {
        this.mailModel.set('security', this.toJson())
        if (!silent) this.mailModel.save()
      }
    }
  },
  setInline: function () {
    this.set('PGPFormat', 'inline')
  },
  setMime: function () {
    this.set('PGPFormat', 'mime')
  }
})

export default SecurityModel
