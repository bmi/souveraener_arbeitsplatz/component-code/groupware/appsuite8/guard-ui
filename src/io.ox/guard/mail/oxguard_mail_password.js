/**
 * All content on this website (including text, images, source
 * code and any other original works), unless otherwise noted,
 * is licensed under a Creative Commons License.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 *
 * Copyright (C) 2016-2020 OX Software GmbH
 */

import $ from '$/jquery'
import _ from '$/underscore'
import PasswordView from '@/io.ox/guard/core/passwordView'
import guardModel from '@/io.ox/guard/core/guardModel'
import passwordPrompt from '@/io.ox/guard/core/passwordPrompt'
import core from '@/io.ox/guard/oxguard_core'
import openSettings from '$/io.ox/settings/util'
import gt from 'gettext'
import '@/io.ox/guard/mail/style.scss'

/// /////////////////////   RECEIVE / READ

// Unified inbox combines folder and email ID, returns email id
function getId (baton) {
  let id = decodeURIComponent(baton.data.id)
  try {
    id = id.substring(id.lastIndexOf('/') + 1) + (baton.view.cid ? baton.view.cid : '')
  } catch (e) {
    console.error(e)
  }
  return (id)
}

function mailPasswordPrompt (baton, badpass, location, goFunction, localOnly) {
  const id = getId(baton)
  const passwordBoxView = new PasswordView.view({ id: 'oxgrpass' + id, class: 'password_prompt', validate: false })
  const passwordbox = passwordBoxView.getProtected().css('display', 'inline')
  const grdPasswordPrompt = $('<div class="alert og_password"/>')
  // #. %s product Name
  const mainpassprompt = $('<label style="font-weight:bold;" for="oxgrpass' + id + '">' + gt('Secure Email, enter your %s security password.', guardModel().getName()) + '</span><br/><div style="height:10px;"/>')
  if (_.device('small')) {
    passwordbox.css('width', '150px')
  }
  const noSaveWorkAround = $('<input style="display:none" type="text" name="dontremember"/><input style="display:none" type="password" name="dontrememberpass"/>')
  const passButton = $('<button class="btn btn-primary oxguard_passbutton" type="button">' + gt('OK') + '</button><br/>')
  const placeholder = $('<span></span><br/>')
  // #. 'Keep me logged into guard' is followed by dropdown box specifying number of minutes or indefinite, %s product name

  const rememberpass = $('<div>').append(passwordPrompt.renderTimeOptions(baton.model, 'duration', gt('Duration')))

  const forgotDiv = $('<div class="og_forgot" style="display:none">')

  if (guardModel().get('recoveryAvail')) { // Display password help if available
    const forgotLink = $('<a href="#">').append(gt('Forgot Password'))
    forgotLink.click(function (e) {
      e.preventDefault()
      openSettings('virtual/settings/io.ox/guard')
    })
    forgotDiv.append(forgotLink)
  }

  const cont = $('<br/><div class="oxguard_error" id = "error' + id + '" style="display:none;"></div>')

  passButton.click(function () {
    go()
  })

  grdPasswordPrompt.append(noSaveWorkAround)
    .append(mainpassprompt.append(passwordbox).append(passButton))
    .append(placeholder)
    .append(rememberpass)
    .append(forgotDiv)
    .append(cont)

  grdPasswordPrompt.keydown(function (e) {
    if (e.which === 13) {
      go()
    }
  })

  window.setTimeout(function () {
    passwordbox.removeAttr('readonly')
  }, 500)

  const contdiv = $('<div class="content" id = "content' + id + '"/>').append(grdPasswordPrompt)

  function go () {
    const password = passwordBoxView.getValue()
    if (password === undefined) return
    let duration = -1
    const model = baton.model
    if (model.get('rememberpass')) {
      duration = model.get('duration')
    }
    if (localOnly) {
      goFunction(password, duration)
      return
    }
    passwordbox.parent().busy()
    passButton.prop('disabled', true)
    core.savePassword(password, duration)
      .done(function () {
        goFunction(password)
      })
      .fail(function (data) {
        $('#busygif').hide()
        passwordbox.parent().idle()
        passButton.prop('disabled', false)
        import('$/io.ox/core/notifications').then(({ default: notifications }) => {
          if (data.error && data.code === 'GRD-MW-0005') {
            notifications.yell('error', gt('Temporary Lockout'))
          } else {
            notifications.yell('error', gt('Bad password'))
            if (guardModel().get('recoveryAvail')) $('.og_forgot').show()
          }
        })
      })
  }
  return (contdiv)
}

export default {
  passwordPrompt: mailPasswordPrompt,
  getId: getId
}
