/* All content on this website (including text, images, source
 * code and any other original works), unless otherwise noted,
 * is licensed under a Creative Commons License.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 *
 * Copyright (C) 2016-2020 OX Software GmbH
 */

import $ from '$/jquery'
import ox from '$/ox'
import _ from '$/underscore'
import core from '@/io.ox/guard/oxguard_core'
import capabilities from '$/io.ox/core/capabilities'
import auth from '@/io.ox/guard/api/auth'
import guardModel from '@/io.ox/guard/core/guardModel'

let firstload = true

// Do the check if the email is PGP email.  Check for inline, MIME, or signature
function checkPGP (baton, location) {
  if (baton.data.security && baton.data.security.decrypted) {
    if (!guardModel().hasGuardMail() && !capabilities.has('guest')) { // If not guard user, display upsell if any
      const loc = baton.view.$el
      if (loc.find('.guardUpsell').length < 1) { // Make sure not already drawn
        import('@/io.ox/guard/mail/upsell').then(({ default: upsell }) => {
          upsell(loc)
        })
      }
    }
    return // If already decoded
  }
  if (baton.view.model === null) {
    return
  }
  const headers = baton.view.model.get('headers')
  if (headers === undefined) { // not yet loaded
    return
  }
  // Init
  if (baton.data.headers === undefined) { // If email wasn't fully loaded into baton, do so now.
    baton.data = baton.view.model.toJSON()
  }
  const mail = baton.data
  if (mail.results !== undefined) return

  if (headers['X-OXGUARD-GUEST'] !== undefined) {
    baton.view.model.set('pgp_found', true)
    pgpAttach(baton)
    pgpFound.call(this, baton)
  }
  // Main check if Guard email
  if (baton.view.model.get('security') && baton.view.model.get('security').decrypted) return // Bug 53622, loop in mobile view
  if (mail.security_info && mail.security_info.encrypted) {
    baton.view.model.set('pgp_found', true)
    pgpAttach(baton)
    pgpFound.call(this, baton, location)
  } else if (mail.security_info && mail.security_info.signed) {
    if (!mail.security && !baton.signatureCheck) pullAgain(false, baton) // If we don't already have result, reload
    baton.signatureCheck = true // avoid loops
  }
}

function showError (data, baton) {
  auth.resetToken()
  core.showError(data)
  if (!baton.signatureCheck) baton.view.redraw() // avoid loop
}

function pullAgain (decrypt, baton) {
  import('$/io.ox/mail/api').then(({ default: api }) => {
    const options = {
      cache: false
    }
    const obj = {
      id: baton.data.id,
      folder_id: baton.data.folder_id
    }
    if (decrypt) {
      obj.decrypt = 'true'
    } else {
      obj.verify = 'true'
    }
    api.get(obj, options)
      .then(function (data) {
        data = removeSignatureFile(data) // remove verified signatures
        if (baton.sessionKey) { // If local decrypt, save the sessionKey for attachments, etc
          data.security.sessionKey = baton.sessionKey
        }
        const origData = _.extend(baton.data, {})
        baton.model.set(data)
        origData.view = baton.view
        baton.model.set('origMail', origData)
        // Update the pool mail with the attachment and security data of the email.  Otherwise will
        // be overwritten with changes in listViews, sort order, etc
        const stored = api.pool.get('detail').get(_.cid(data))
        if (stored) {
          stored.set('guardIcons', _.pick(data, 'attachment', 'security'))
        }
        delete (baton.view.attachmentView) // Need to redo the attachment list
        baton.view.redraw()
      }, function (data) {
        showError(data, baton)
      })
  })
}

// Remove signature.asc files from attachments if signatures verified
function removeSignatureFile (data) {
  if (data.security && data.security.signatures) {
    let verified = true
    data.security.signatures.forEach(function (d) {
      verified = verified && d.verified
    })
    if (verified) {
      const newAttachments = []
      data.attachments.forEach(function (att) {
        if (att.content_type !== 'application/pgp-signature') {
          newAttachments.push(att)
        }
      })
      data.attachments = newAttachments
    }
  }
  return data
}

function busy (location) {
  $(location).busy()
}

// PGP found, do send for decode
function pgpFound (baton, location) {
  const goFunction = function () {
    pullAgain(true, baton)
  }
  checkLoaded().then(function () {
    checkSession().then(function (auth) {
      import('@/io.ox/guard/mail/oxguard_mail_password').then(({ default: password }) => {
        if (auth && auth.length > 10) { // If auth code in session, load decrypted
          pullAgain(true, baton)
        } else if (guardModel().hasAuth() || guardModel().notReady()) { // If we have auth, or needs further action
          busy(location, password.getId(baton))
          checkauth(goFunction, baton, undefined)
        } else {
          // Otherwise, draw password prompt
          $(location).replaceWith(password.passwordPrompt(baton, false, location, goFunction))
          baton.view.$el.find('.attachments').hide()
          if (!_.device('ios')) {
            baton.view.$el.find('#oxgrpass').focus()
          }
          baton.view.$el.show()
        }
      })
    })
  })
}

function checkSession () {
  const def = $.Deferred()
  if (!firstload) {
    def.resolve()
    return def
  }
  core.checkAuth()
    .done(function (data) {
      firstload = false
      def.resolve(data.auth)
    })
    .fail(function () {
      firstload = false
      def.resolve()
    })
  return def
}

// Timeout loop waiting for the Guard backend data to be loaded
function waitForLoad (def) {
  console.debug('Waiting for Guard load')
  if (guardModel().isLoaded()) {
    def.resolve()
    return
  }
  setTimeout(function () {
    waitForLoad(def)
  }, 500)
}

// Check if Guard backend data loaded.  If not, start wait loop
function checkLoaded () {
  const def = $.Deferred()
  if (guardModel().isLoaded()) {
    def.resolve()
  } else {
    waitForLoad(def)
  }
  return def
}

function checkauth (go, baton, index) {
  if (guardModel().needsPassword()) { // If no password for user has been defined, open dialog
    import('@/io.ox/guard/core/tempPassword').then(({ default: temp }) => {
      temp.createOxGuardPasswordPrompt(baton, go, '', undefined, index, location)
    })

    return
  }
  if (guardModel().needsKey()) { // If received a first encrypted email while already logged in, will still have 'no key' in passcode
    import('@/io.ox/guard/core/tempPassword').then(({ default: temp }) => {
      core.auth(ox.user_id, '') // Try to reload authorization to see if keys now exist
        .done(function (data) {
          guardModel().loadData(data)
          if (data.auth === 'No Key') {
            baton.model.unset('security_info')
            baton.view.redraw()
            return // Still no key, non guard generated email?
          }
          temp.createOxGuardPasswordPrompt(baton, go, '', undefined, index, location) // Create new password
        })
    })
    return
  }
  go(baton, '', index, guardModel().getAuth())
}

function pgpAttach (baton) {
  baton.data.PGP = true
}

export { checkPGP, pullAgain, showError, pgpFound }
