/**
 * This work is provided under the terms of the CREATIVE COMMONS PUBLIC
 * LICENSE. This work is protected by copyright and/or other applicable
 * law. Any use of the work other than as authorized under this license
 * or copyright law is prohibited.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 *
 *  2016-2020 OX Software GmbH, Tarrytown, NY, USA. info@open-xchange.com
 *
 */

import $ from '$/jquery'
import userAPI from '$/io.ox/core/api/user'
import capabilities from '$/io.ox/core/capabilities'
import { settings } from '$/io.ox/mail/settings'
import guardModel from '@/io.ox/guard/core/guardModel'

const api = {}

/**
 * Test if guard is configured.
 *
 * @return true if configured; false otherwise
 */
api.isGuardConfigured = function () {
  if (!guardModel().isLoaded()) return false // Make sure Guard response
  return !guardModel().needsKey()
}

api.hasSetupDone = function () {
  const def = $.Deferred()
  if (guardModel().needsKey()) {
    def.reject()
  } else def.resolve()
  if (guardModel().needsPassword()) {
    import('@/io.ox/guard/core/tempPassword').then(({ default: tempPass }) => {
      tempPass.createOxGuardPasswordPrompt()
    })
  }
  return def
}

api.hasCryptoCapability = function () {
  return (capabilities.has('guard'))
}

api.hasGuardMailCapability = function () {
  return (capabilities.has('guard-mail'))
}

api.isGuest = function () {
  return capabilities.has('guest')
}

api.isGuardLoaded = function () {
  return guardModel().isLoaded()
}

api.addOnLoaded = function (loadFunction) {
  if (!guardModel().get('onload')) {
    guardModel().set('onload', [])
  }
  guardModel().get('onload').push(loadFunction)
}

api.hasStoredPassword = function () {
  return (guardModel().hasAuth())
}

api.format = function (fingerprint) {
  return (fingerprint || '').match(/.{1,4}/g).join(' ')
}

api.autoCryptEnabled = function () {
  return (guardModel().getSettings().autoCryptEnabled)
}

api.getUsersPrimaryEmail = function () {
  return $.when(settings.ensureData().then(() => {
    const defaultSendAddress = $.trim(settings.get('defaultSendAddress', ''))
    if (!defaultSendAddress) return $.when(defaultSendAddress)
    return userAPI.get().then(function (userData) {
      return userData.email1
    })
  }))
}

api.sanitize = function (data) {
  const regex = new RegExp('<(?:!--(?:(?:-*[^->])*--+|-?)|script\\b(?:[^"\'>]|"[^"]*"|\'[^\']*\')*>[\\s\\S]*?</script\\s*|style\\b(?:[^"\'>]|"[^"]*"|\'[^\']*\')*>[\\s\\S]*?</style\\s*|/?[a-z](?:[^"\'>]|"[^"]*"|\'[^\']*\')*)>', 'gi')
  return (data.replace(regex, ''))
}

api.validateEmail = function (email) {
  const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  return re.test(email)
}

export default api
