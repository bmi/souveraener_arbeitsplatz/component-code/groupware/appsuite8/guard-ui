/**
 * All content on this website (including text, images, source
 * code and any other original works), unless otherwise noted,
 * is licensed under a Creative Commons License.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 *
 * Copyright (C) 2016-2020 OX Software GmbH
 */

function parseResponse (resp) {
  try {
    if (typeof resp === 'string' || resp instanceof String) {
      resp = resp.trim()
      if (resp.startsWith('{') && resp.endsWith('}')) {
        return JSON.parse(resp)
      }
      if (resp.startsWith('<!DOCTYPE')) {
        const i = resp.indexOf('{')
        if (i > 0) {
          resp = resp.substring(i)
          const j = resp.indexOf('}')
          if (j > 0) {
            resp = resp.substring(0, j + 1)
            return JSON.parse(resp)
          }
        }
      }
    }
    return ({})
  } catch (e) {
    return ({})
  }
}

export default {
  parseResponse: parseResponse
}
