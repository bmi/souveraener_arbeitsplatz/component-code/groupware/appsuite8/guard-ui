/**
 * All content on this website (including text, images, source
 * code and any other original works), unless otherwise noted,
 * is licensed under a Creative Commons License.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 *
 * Copyright (C) 2016-2020 OX Software GmbH
 * Author Greg Hill <greg.hill@open-xchange.com>
 */

import $ from '$/jquery'
import _ from '$/underscore'
import Backbone from '$/backbone'
import auth from '@/io.ox/guard/auth'
import gt from 'gettext'
import '@/io.ox/guard/pgp_mail/style.scss'
import guardModel from '@/io.ox/guard/core/guardModel'
import { settings } from '@/io.ox/guard/settings/guardSettings'
import icons from '@/io.ox/guard/core/icons'

const ToggleEncryptionView = Backbone.View.extend({
  tagName: 'a',
  className: 'toggle-encryption',
  initialize: function (options) {
    this.mailModel = options.mailModel
    this.listenTo(this.model, 'change:encrypt', function (model, val) {
      this.$('i.fa').toggleClass('encrypted', val)
      // gt('Click to enable encryption')
      // gt('Click to disable encryption')
    })
  },
  events: {
    click: 'toggle',
    'change:encrypt': 'changed'
  },
  toggle: function (e) {
    e.preventDefault()
    if (this.encryption_forced) {
      import('$/io.ox/core/notifications').then(({ default: notify }) => {
        notify.yell('error', gt('Reply must be encrypted'))
      })
    } else {
      const model = this.model
      if (!model.get('encrypt')) {
        auth.ensureSetup().then(function () {
          model.set('encrypt', !model.get('encrypt'))
          if (!model.get('encrypt')) { // If removing encryption, check for pgp attachments
            checkPGPAttachment(this.mailModel)
          }
        })
      } else {
        model.set('encrypt', !model.get('encrypt'))
      }
    }
  },
  isEnabled: function () {
    return guardModel().hasGuardMail() || this.encryption_forced
  },
  forceEncryption: function () {
    this.encryption_forced = true
    this.model.set('encrypt', true)
    this.render()
  },
  render: function () {
    this.$el.attr('tabindex', '-1')
    if (this.isEnabled()) {
      if (!_.device('small')) {
        this.$el.empty().append(
          getLockIcon(this.model.get('encrypt'))
        )
        this.$el.attr('title', gt('Enable Encryption')).attr('href', '#').attr('role', 'button')
      }
    }
    this.listenTo(this.model, 'change:encrypt', function () {
      this.changed()
    })
    if (this.model.get('encrypt')) {
      const view = this.view
      window.setTimeout(function () {
        showAttLink(view, false)
      }, 100)
    }
    return this
  },
  noLinkMail: function (view) {
    this.view = view
    checkAttLinkView(this.mailModel, view)
  },
  changed: function () {
    this.$el.empty()
      .append(getLockIcon(this.model.get('encrypt')))
      .attr('title', this.model.get('encrypt') ? gt('Disable Encryption') : gt('Enable Encryption'))
    if (this.model && this.model.get('encrypt')) {
      showAttLink(this.view, false)
      checkAttLink(this.mailModel)
      try {
        const fromArray = this.mailModel.get('from')
        const from = _.isArray(fromArray[0]) ? fromArray[0][1] : fromArray[1]
        if (fromArray && guardModel().get('primaryEmail') !== undefined) {
          if (guardModel().get('primaryEmail') !== from) {
            const warn = settings.get('warnEmailDifferent')
            if (warn === undefined || warn === false) {
              displayWarning()
            }
          }
        }
      } catch (e) {
        console.log(e)
      }
    } else {
      showAttLink(this.view, true)
    }
  }
})

/*
// TODO-784: get rid of
const ToggleEncryptionViewMobile = Backbone.View.extend({
  tagName: 'button',
  initialize: function (model, mailModel) {
    this.mailModel = mailModel
    this.listenTo(this.model, 'change:encrypt', function (model, val) {
      this.$('i.fa').toggleClass('encrypted', val)
      // gt('Click to enable encryption')
      // gt('Click to disable encryption')
    })
  },
  className: 'toggle-encryption',
  events: {
    click: 'toggle',
    'change:encrypt': 'changed'
  },
  toggle: function () {
    if (this.encryption_forced) {
      import('$/io.ox/core/notifications').then(({ default: notify }) => {
        notify.yell('error', gt('Reply must be encrypted'))
      })
    } else {
      const model = this.model
      if (!model.get('encrypt')) {
        auth.ensureSetup().then(function () {
          model.set('encrypt', !model.get('encrypt'))
          if (!model.get('encrypt')) { // If removing encryption, check for pgp attachments
            checkPGPAttachment(this.mailModel)
          }
        }), baton.model
      } else {
        model.set('encrypt', !model.get('encrypt'))
      }
    }
  },
  forceEncryption: function () {
    this.encryption_forced = true
    this.model.set('encrypt', true)
    this.render()
  },
  render: function () {
    this.$el.attr('tabindex', '-1')
    const guard = guardModel().hasGuardMail()
    if (guard || this.encryption_forced) {
      if (!_.device('small')) {
        this.$el.empty().append(
          icons.getIcon({ name: 'lock-fill' })
            .toggleClass('encrypted', !!this.model.get('encrypt'))
        )
        this.$el.attr('title', gt('Enable Encryption'))
      }
    }
    this.listenTo(this.model, 'change:encrypt', function () {
      this.changed()
    })
    if (this.model.get('encrypt')) {
      const view = this.view
      window.setTimeout(function () {
        showAttLink(view, false)
      }, 100)
    }
    return this
  },
  noLinkMail: function (view) {
    this.view = view
    checkAttLinkView(this.mailModel, view)
  },
  changed: function () {
    this.$el.empty()
      .append(getLockIcon(this.model.get('encrypt')))
      .attr('title', this.model.get('encrypt') ? gt('Disable Encryption') : gt('Enable Encryption'))
    if (this.model && this.model.get('encrypt')) {
      showAttLink(this.view, false)
      checkAttLink(this.mailModel)
      try {
        const fromArray = this.mailModel.get('from')
        const from = _.isArray(fromArray[0]) ? fromArray[0][1] : fromArray[1]
        if (fromArray && guardModel().get('primaryEmail') !== undefined) {
          if (guardModel().get('primaryEmail') !== from) {
            const warn = settings.get('warnEmailDifferent')
            if (warn === undefined || warn === false) {
              displayWarning()
            }
          }
        }
      } catch (e) {
        console.log(e)
      }
    } else {
      showAttLink(this.view, true)
    }
  }
})

*/

function getLockIcon (encrypted) {
  return encrypted ? icons.getLocked() : icons.getUnLocked()
}

// Hide or show mail attachment link
function showAttLink (view, show) {
  if (view) {
    view.$('.attachments').toggleClass('guard-encrypted', !show)
  }
}

// Check if Mail Attachment link is shown and encryption enabled
function checkAttLinkView (model, view) {
  checkAttLink(model)
  if (model.get('encrypt')) {
    showAttLink(view, false)
  }
}

// Show a warning if Mail Attachment Link is shown and encryption clicked
function checkAttLink (model) {
  if (model.get('sharedAttachments')) {
    if (model.get('sharedAttachments').enabled) {
      Promise.all([import('$/io.ox/mail/settings'),
        import('$/io.ox/backbone/views/modal')])
        .then(function ([{ settings: mailSettings }, { default: ModalDialog }]) {
          const dialog = new ModalDialog({
            async: true,
            point: 'io.ox/guard/pgp_mail/shareAttError',
            title: gt('Not Supported'),
            id: 'attError',
            width: 400
          })
            .extend({
              explanation: function () {
                const text = $('<p>').text(gt('%s is not supported with secured email and will be disabled.', mailSettings.get('compose/shareAttachments/name')))
                this.$body.append(text)
              }
            })
            .addButton({ label: gt('OK'), action: 'ok' })
            .on('ok', function () {
              $('.share-attachments').find(':checkbox').prop('checked', false)
              const share = model.get('sharedAttachments')
              share.enabled = false
              model.set('sharedAttachments', share)
              dialog.close()
            })
            .open()
        })
    }
  }
}

// Warning that the from address is different from primary
function displayWarning () {
  import('$/io.ox/backbone/views/modal').then(({ default: ModalDialog }) => {
    new ModalDialog({
      async: true,
      point: 'io.ox/guard/pgp_mail/otherSendFrom',
      title: gt('Warning'),
      id: 'otherSendFrom',
      width: 450
    })
      .extend({
        explanation: function () {
          const text = $('<p>').text(gt('Sending an encrypted email from an account other than %s may cause problems with the recipient being able to reply.  Consider using your primary account.', guardModel().get('primaryEmail')))
          const warn = $('<input type="checkbox" id="warnEmail">')
          const warntext = $('<span class="selectable" style="padding-left:10px;">').text(gt('Do not warn me again'))
          warntext.click(function () {
            warn.click()
          })
          warn.click(function (e) {
            settings.set('warnEmailDifferent', $(e.target).is(':checked'))
            settings.save()
          })
          this.$body.append(text).append('<hr>').append(warn).append(warntext)
        }
      })
      .addButton({ label: gt('OK'), action: 'ok' })
      .on('ok', function () {
        this.close()
      })
      .open()
  })
}

// Check to see if there are pgp attachments.  Checked if encryption removed
// Likely the recipient won't be able to open this attachment if not sent through Guard
function checkPGPAttachment (model) {
  const attachments = model.get('attachments')
  if (attachments && _.isArray(attachments.models)) {
    attachments.models.forEach(function (attModel) {
      if (attModel.get('file-mimetype') === 'application/pgp-encrypted' ||
                    (attModel.get('filename') && attModel.get('filename').indexOf('.pgp') > 0)) {
        import('$/io.ox/core/yell').then(({ default: yell }) => {
          // #.  Warning to the user after they removed encryption from an email.  One of the attachments is a pgp item, and it will likely be unreadable to the recipient if they remove encryption
          yell('warning', gt('Sending encrypted attachments without encrypting the email will usually result in the attachment being unreadable by the recipient.'))
        })
      }
    })
  }
}

export default {
  View: ToggleEncryptionView
}
