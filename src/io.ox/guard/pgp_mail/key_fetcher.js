/**
 * All content on this website (including text, images, source
 * code and any other original works), unless otherwise noted,
 * is licensed under a Creative Commons License.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 *
 * Copyright (C) 2016-2020 OX Software GmbH
 * Author Greg Hill <greg.hill@open-xchange.com>
 */

import $ from '$/jquery'
import ox from '$/ox'
import http from '@/io.ox/guard/core/og_http'
import gt from 'gettext'

const checking = []

function checkKey (email) {
  if (checking.indexOf(email) > -1) return ($.Deferred().reject())
  checking.push(email)
  return (getKey(email, false))
}

function downloadKey (email) {
  return (getKey(email, true))
}

// Query Guard server for PGP keys.  Get key if download, else get info on keys
function getKey (email, download) {
  const def = $.Deferred()
  const params = '&email=' + email +
        '&cid=' + ox.context_id +
        '&userid=' + ox.user_id +
        '&createIfMissing=true' +
        (download === true ? '&download=true' : '')

  http.get(ox.apiRoot + '/oxguard/pgpmail?action=getkey', params)
    .done(function (respData) {
      if (respData !== '') {
        let data
        if (typeof respData === 'string') {
          data = JSON.parse(respData).data
        } else {
          data = respData.data
        }
        if (data === 'guest') {
          def.reject()
        }
        if (data === '') {
          def.reject()
        }
        def.resolve(data)
        return
      }
      def.reject()
    })
    .fail(function () {
      def.reject()
    })
    .always(function () {
      // checking.pop(email);
    })
  return (def)
}

function promptKeyImport (data, email) {
  const def = $.Deferred()
  Promise.all([import('@/io.ox/guard/pgp/keyDetails'),
    import('$/io.ox/backbone/views/modal').then(({ default: publickeys }, { default: ModalDialog }) => {
      new ModalDialog({
        async: true,
        point: 'io.ox/guard/pgp_mail/importKey',
        title: gt('Key found for %1$s.  Import?', email),
        id: 'importKey',
        width: 450,
        def: def
      })
        .extend({
          body: function () {
            const keydetail = publickeys.keyDetail(data)
            this.$body.append(keydetail)
          }
        })
        .addButton({ label: gt('Import'), action: 'import' })
        .addCancelButton()
        .on('cancel', function () {
          this.options.def.reject()
        })
        .on('import', function () {
          const that = this
          downloadKey(email).done(function (data) {
            that.options.def.resolve(data)
          })
            .fail(function () {
              that.options.def.reject()
            })
          that.close()
        })
        .open()
    })
  ])

  return (def)
}

export default {
  checkKey: checkKey,
  downloadKey: downloadKey,
  promptKeyImport: promptKeyImport
}
