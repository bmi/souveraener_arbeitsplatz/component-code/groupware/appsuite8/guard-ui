/**
 * All content on this website (including text, images, source
 * code and any other original works), unless otherwise noted,
 * is licensed under a Creative Commons License.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 *
 * Copyright (C) 2016-2020 OX Software GmbH
 * Author Greg Hill <greg.hill@open-xchange.com>
 */

import Backbone from '$/backbone'
import $ from '$/jquery'
import ext from '$/io.ox/core/extensions'
import icons from '@/io.ox/guard/core/icons'
import '@/io.ox/guard/pgp_mail/style.scss'

const RecipientModel = Backbone.Model.extend({
  initialize: function () {
    this.listenTo(this, 'change:email', this.lookup)
    // do initial lookup
    this.lookup(this, this.get('email'))
  },
  defaults: {
    email: '',
    keys: []
  },
  addKey: function (obj) {
    this.set('keys', this.get('keys').concat(obj))
  },
  lookup: function (model, val) {
    if (!val) return

    const baton = new ext.Baton({
      email: val
    })
    ext.point('pgp_mail/keyring/lookup').invoke('action', model, baton)
  }
})

const RecipientView = Backbone.View.extend({
  initialize: function (opt) {
    this.renderAddress = opt.renderAddress
    this.listenTo(this.model, 'change:keys', function () {
      this.render()
    })
  },
  tagName: 'span',
  className: 'recipient-state',
  render: function () {
    const state = icons.getIcon({ name: 'key' })
    let address = ''
    if (this.renderAddress) {
      address = $('<span class="email">').text(this.model.get('email'))
    }
    state.toggleClass('key-found', this.model.get('keys').length > 0)
    const trusted = this.model.get('keys').reduce(function (acc, key) {
      return acc || key.trusted === true
    }, false)
    state.toggleClass('trusted', trusted)
    this.$el.empty().append(address, state)
    return this
  }
})

const recipients = {
  View: RecipientView,
  Model: RecipientModel
}

export default {
  recipients: recipients
}
