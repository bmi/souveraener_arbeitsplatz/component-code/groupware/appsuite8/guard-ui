/**
 * This work is provided under the terms of the CREATIVE COMMONS PUBLIC
 * LICENSE. This work is protected by copyright and/or other applicable
 * law. Any use of the work other than as authorized under this license
 * or copyright law is prohibited.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 *
 * © 2016 OX Software GmbH, Germany. info@open-xchange.com
 *
 * @author Greg Hill <greg.hill@open-xchange.com>
 */

import $ from '$/jquery'
import Backbone from '$/backbone'
import ext from '$/io.ox/core/extensions'

/// /////// Extension point portal

ext.point('io.ox/portal/widget/myfiles').extend({
  id: 'OxGuardRecentFiles',
  index: 1000000,
  preview: function () {
    const content = $(this).find('.list-unstyled')
    setupGuard(content)
  }
})

ext.point('io.ox/portal/widget/recentfiles').extend({
  id: 'OxGuardRecentFiles',
  index: 1000000,
  preview: function () {
    const content = $(this).find('.list-unstyled')
    setupGuard(content)
  }
})

function setupGuard (content) {
  content.unbind('click')
  content.on('click', 'li.item', function (e) {
    e.stopPropagation()
    const item = $(e.currentTarget).data('item')
    if (isEncrypted(item)) {
      Promise.all([import('@/io.ox/guard/files/register'),
        import('$/io.ox/files/api')])
        .then(function ([{ default: files }, { default: filesAPI }]) {
          const items = $(e.delegateTarget).data('items')
          filesAPI.get(item).done(function (data) {
            const models = filesAPI.resolve(items, false)
            const collection = new Backbone.Collection(models)
            const baton = new ext.Baton({ data: data, collection: collection })
            files.viewFile(baton)
          })
        })
    } else {
      const items = $(e.delegateTarget).data('items')
      Promise.all([import('$/io.ox/core/viewer/main'),
        import('$/io.ox/files/api')])
        .then(function ([{ default: Viewer }, { default: filesAPI }]) {
          filesAPI.get(item).done(function (data) {
            const models = filesAPI.resolve(items, false)
            const collection = new Backbone.Collection(models)
            const viewer = new Viewer()
            const baton = new ext.Baton({ data: data, collection: collection })
            viewer.launch({ selection: baton.data, files: baton.collection.models })
          })
        })
    }
  })
}

function isEncrypted (item) {
  if (item.meta) {
    if (item.meta.Encrypted === true) return (true)
  }
  if (item.filename) {
    if (item.filename.indexOf('.pgp') > 0) return (true)
  }
  return (false)
}
