/* All content on this website (including text, images, source
 * code and any other original works), unless otherwise noted,
 * is licensed under a Creative Commons License.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 *
 * Copyright (C) 2016-2020 OX Software GmbH
 */

'use strict'

import $ from '$/jquery'
import _ from '$/underscore'
import ext from '$/io.ox/core/extensions'
import { Action } from '$/io.ox/backbone/views/actions/util'
import http from '@/io.ox/guard/core/og_http'
import * as checker from '@/io.ox/guard/mail/checker'
import account from '$/io.ox/core/api/account'
import capabilities from '$/io.ox/core/capabilities'
import * as util from '@/io.ox/guard/pgp_mail/util'
import auth_core from '@/io.ox/guard/auth'
import guardModel from '@/io.ox/guard/core/guardModel'
import notify from '$/io.ox/core/notifications'
import mailMain from '$/io.ox/mail/main'
import { settings } from '@/io.ox/guard/settings/guardSettings'
import icons from '@/io.ox/guard/core/icons'
import ox from '$/ox'
import gt from 'gettext'
import '@/io.ox/guard/pgp/style.scss'

function drawIcons (baton) {
  if (!baton.model.get('security')) return
  const security = baton.model.get('security')
  if (security.decrypted) {
    const pgp = $('<span class="oxguard_icon_fa" title="' + gt('This Email was encrypted with PGP.') + '"/>')
    pgp.append('P<span style="font-size:0.5em;">gp</span>')
    const lockicon = icons.getIcon({ name: 'lock-fill', className: 'pgp_superscript icon-lock' })
    pgp.append(lockicon)
    if (_.device('small')) {
      pgp.css('margin-right', '5px')
    }
    this.append(pgp)
  }
  if (security.signatures) {
    let verified = true
    let error
    let missing = false
    security.signatures.forEach(function (sig) {
      verified = verified && sig.verified
      if (sig.missing) missing = true
      if (sig.error) error = sig.error
    })
    const signedicon = $('<span class="oxguard_icon_fa">')
    if (verified) {
      signedicon.append(icons.getIcon({ name: 'pencil-square', className: 'oxguard_icon_fa guard_signed' })).attr('title', gt('This Email was signed and verified.'))
    } else if (!missing && !error) {
      signedicon.append(icons.getIcon({ name: 'x-circle', className: 'oxguard_icon_fa_error guard_signed' })).attr('title', gt('This Email was signed and failed verification.'))
    }
    if (missing) {
      signedicon.append(icons.getIcon({ name: 'pencil-square', className: 'guard_signed' }).css('color', 'lightgrey')).attr('title', gt('This Email was signed but unable to verify.'))
    }
    if (error) {
      signedicon.append(icons.getIcon({ name: 'x-circle', className: 'oxguard_icon_fa_error guard_signed' })).attr('title', gt('Error verifying signature: %s', error))
    }
    if (_.device('small')) {
      signedicon.css('margin-right', '10px')
    }
    this.append(signedicon)
  }
  checkIntegrity(baton.data)
}

ext.point('io.ox/mail/mobile/detail/header/flags').replace('security', function () {
  return {
    draw: drawIcons
  }
})

ext.point('io.ox/mail/detail/header/row1').replace('security', function () {
  return {
    draw: drawIcons
  }
})

function checkIntegrity (data) {
  if (data && data.headers) {
    if (data.headers['X-Guard-Failed-Integrity']) {
      notify.yell('warning', gt('This email is missing one or more security checks.  HTML markup was removed for your security.'))
    }
  }
}

// If so configured, cleanup any opened Guard emails
function cleanGuardCache (baton) {
  if (settings.get('wipe') !== 'true') { // If not configured to do so, exit
    return
  }
  if (guardModel().getAuth()) { // Don't wipe if still authenticated
    return
  }
  import('$/io.ox/mail/api').then(({ default: mailApi }) => {
    if (!mailApi.pool) return
    if (baton.options && baton.options.name === 'io.ox/mail/compose') { // Don't wipe when just opening compose
      return
    }
    const collection = mailApi.pool.get('detail')
    if (collection && collection.models) {
      collection.models.forEach(function (model) {
        const security = model.get('security')
        if (security && security.decrypted) {
          let found = false
          if (baton.model) { // Still within mail application
            if (model.get('cid') === baton.model.cid) { // Don't wipe the active mail
              found = true
            }
            const mailApp = mailMain.getApp()
            if (mailApp.isThreaded()) {
              mailApp.threadView.collection.models.forEach(function (thrModel) {
                if (thrModel.get('cid') === model.cid) {
                  found = true
                }
              })
            }
          }
          if (!found && model.get('origMail')) {
            const orig = model.get('origMail')
            model.clear()
            model.set(orig)
            const view = model.get('view')
            if (view.$el) { // when changing apps, need to redraw
              view.redraw()
            }
          }
        }
      })
    }
  })
}

if (capabilities.has('webmail')) { // Register mail cleanup only if has mail
  ox.on('app:start', cleanGuardCache)
  ox.on('app:resume', cleanGuardCache)
}

ext.point('io.ox/mail/detail/header').extend({
  id: 'guardNotices',
  index: 'last',
  draw: function (baton) {
    if (settings.get('suppressInfo')) {
      return
    }
    const security = baton.model.get('security')
    if (security) {
      let warn
      const div = $('<div class="oxguard_info">')
      if (security && security.signatures) {
        const sig = security.signatures[0]
        if (sig && sig.missing) {
          div.append($('<span>').append(gt('Unable to verify signature.  Missing public key.')))
          warn = true
        }
        if (sig && !sig.verified && !sig.missing && !sig.error) {
          div.append($('<span class="signError">').append(gt('This Email was signed and failed verification.')))
          warn = true
        }
      }
      if (warn) this.append(div)
    }
  }
})

ext.point('io.ox/mail/detail/attachments').extend({
  id: 'ogCheck',
  index: 1,
  draw: function (baton) {
    if (baton.data.security_info && baton.data.security_info.encrypted) baton.stopPropagation()
  }
})

ext.point('io.ox/mail/detail/body').extend({
  id: 'ogPGPCheck',
  index: 1,
  draw: function (baton) {
    if (this[0] !== undefined) {
      let location
      if (this[0].host !== undefined) { // Chrome
        location = $(this[0].host)
      } else { // Others
        location = this[0]
      }
      checker.checkPGP(baton, location)
    }
    if (baton.data.security_info && baton.data.security_info.encrypted) {
      baton.stopPropagation()
      checkAutocryptHeaders(baton) // Check headers for the encrypted emails
      cleanGuardCache(baton) // Clean previously decrypted emails
    }
  }
})

// Check for autocrypt header
ext.point('io.ox/mail/detail/body').extend({
  requires: 'guard-mail',
  index: 1000,
  id: 'autocrypt',
  draw: function (baton) {
    checkAutocryptHeaders(baton)
  }
})

ext.point('io.ox/mail/mobile/detail/body').extend({
  index: 230,
  id: 'ogPGPCheckMobile',
  draw: function (baton) {
    const location = this[0]
    checker.checkPGP.call(this, baton, location)
    baton.view.listenTo(baton.view.model, 'change:attachments', function () {
      checker.checkPGP(baton, location)
      if (baton.view.$el === null) return
      if (!_.device('ios')) {
        window.setTimeout(function () {
          baton.view.$el.find('#oxgrpass').focus()
        }, 50)
      }
    })
    if (baton.data.security_info && baton.data.security_info.encrypted) {
      baton.stopPropagation()
    }
  }
})

ext.point('io.ox/mail/detail/body').extend({
  index: 'last',
  id: 'cleanup',
  draw: function (baton) {
    cleanGuardCache(baton)
  }
})

//  Wipe previews for encrypted emails
function wipePreview (baton) {
  if (baton.data.text_preview) {
    if (baton.data.content_type && baton.data.content_type.indexOf('encrypted') > 0) {
      baton.data.text_preview = ''
    }
    if (baton.data.text_preview.indexOf('----BEGIN PGP') > -1) {
      baton.data.text_preview = ''
    }
  }
}

ext.point('io.ox/mail/listview/item').extend({
  id: 'guard-preview',
  index: 10,
  draw: wipePreview
})

ext.point('io.ox/mail/view-options').extend({
  id: 'guardThread',
  index: 1,
  draw: function (baton) {
    if (capabilities.has('guest')) {
      baton.app.settings.set('threadSupport', false)
    }
  }
})

// Methods for actions
// Check if attachment filename ends with .asc
function isASC (e) {
  try {
    if (e.context instanceof Array) {
      for (let i = 0; i < e.context.length; i++) {
        if (e.context[i].filename.indexOf('.asc') > 0) {
          // Do not return positive for signatures.  Handled differently
          if (e.context[i].content_type.indexOf('sig') > 0) return false
          return true
        }
      }
    }
    const filename = e.context.filename
    if (filename === undefined) return false
    if (e.context.content_type.indexOf('sig') > 0) return false
    return (filename.indexOf('.asc') > 0)
  } catch (d) {
    console.log(d)
    return false
  }
}

function checkAutocryptHeaders (baton) {
  if (baton.model && baton.model.get('headers')) {
    const headers = baton.model.get('headers')
    if (headers.Autocrypt || headers['Autocrypt-Gossip']) {
      import('@/io.ox/guard/pgp/autocrypt/autoCrypt').then(({ default: autocrypt }) => {
        if (headers.Autocrypt) {
          autocrypt.check(baton.model)
            .done(function () {
              if (!settings.get('advanced')) return // Only users with advanced settings would have verified key
              if (baton.data.security_info && baton.data.security_info.signed) { // If signed, pull again after import
                checker.pullAgain(false, baton)
              }
            })
        }
        if (headers['Autocrypt-Gossip']) {
          autocrypt.checkGossip(baton.model)
        }
      })
    }
  }
}

// test a single file if it is a key
function testKey (file) {
  const test = {
    collection: null,
    context: file
  }
  return (isKey(test))
}

function isKey (e) {
  if (isASC(e)) {
    if (e.collection === null || e.collection.has('one')) { // only return true for individual files
      let type
      let name
      if (e.context instanceof Array) {
        type = e.context[0].content_type
        name = e.context[0].filename
      } else {
        type = e.context.content_type
        name = e.context.filename
      }
      if (type !== undefined) {
        if (type.indexOf('application/pgp-keys') > -1) return true
      }
      if (name === 'public.asc' || name === 'publickey.asc') return true
      return (/[0x]*[0-9A-F]{8}\.asc/i).test(name) // Regex for 8 hex.asc
    }
  }
  return false
}

function testAutoCrypt (file) {
  if (file && file.content_type === 'application/autocrypt-setup') return true
  return false
}

ext.point('io.ox/mail/detail/attachments').extend({
  id: 'Import',
  draw: function (e) {
    if (capabilities.has('guest')) return
    let keyfound = false
    let autoCryptStart = false
    e.attachments.forEach(function (a) {
      if (testKey(a)) {
        keyfound = true
      }
      if (testAutoCrypt(a)) {
        autoCryptStart = true
      }
    })
    if (keyfound) {
      const keydiv = $('<div class="importKey">')
      const notice = $('<span>' + gt('PGP Public Key Found.  Click to Import') + '</span>')
      notice.click(function () {
        doImportKey(e.attachments)
      })
      this.append(keydiv.append(notice))
    }
    if (autoCryptStart) {
      const aCkeydiv = $('<div class="importKey">')
      const aCnotice = $('<span>' + gt('AutoCrypt startup found.  Click to import key') + '</span>')
      aCnotice.click(function () {
        import('@/io.ox/guard/pgp/autocrypt/autoCrypt').then(({ default: autocrypt }) => {
          autocrypt.doStart(e.attachments)
        })
      })
      this.append(aCkeydiv.append(aCnotice))
    }
  }
})

new Action('io.ox/mail/actions/save-encrypted-attachment', {
  capabilities: 'infostore guard-drive',
  matches: function (baton) {
    return util.isDecrypted(baton.first())
  },
  collection: 'some',
  action: function (baton) {
    import('$/io.ox/mail/actions/attachmentSave').then((action) => {
      const options = {
        optPrompt: gt('Please re-enter your %s password.', guardModel().getName()),
        minSingleUse: true
      }
      auth_core.authorize(baton.data, options)
        .done(function () {
          const list =
                         _.isArray(baton.data) ? baton.data : [baton.data]
          for (let i = 0; i < list.length; i++) {
            list[i].reEncrypt = true
          }
          action.multiple(list)
        })
    })
  }
})

ext.point('io.ox/mail/attachment/links').extend({
  id: 'saveEncrypted',
  index: 550,
  mobile: 'high',
  // #. %1$s is usually "Drive" (product name; might be
  // customized)
  title: gt('Save encrypted to %1$s', gt.pgettext('app',
    'Drive')),
  ref: 'io.ox/mail/actions/save-encrypted-attachment'
})

function doImportKey (list) {
  if (guardModel().needsKey()) {
    import('@/io.ox/guard/core/createKeys').then((keys) => {
      keys.createKeysWizard()
        .done(function () {
          loadPublicKey(list)
        })
    })
  } else {
    loadPublicKey(list)
  }
}

/// Sent folder extensions

ext.point('io.ox/mail/links/inline').extend({
  index: 101,
  prio: 'lo',
  id: 'pinlink',
  title: gt('Check assigned PIN'),
  ref: 'io.ox/mail/actions/pin',
  mobile: 'lo'
})

const unified_sent = ''

new Action('io.ox/mail/actions/pin', {
  id: 'statusaction',
  matches: function (baton) {
    const e = baton.first()
    if (!(_.contains(account.getFoldersByType('sent'), e.folder_id)) && (e.folder_id !== unified_sent)) return (false)
    try {
      if (e.headers === undefined) return (false)
      return (e.headers['X-OxGuard-PIN'] !== undefined)
    } catch (ex) {
      console.log(ex)
      return (false)
    }
  },
  action: function (baton) {
    pin(baton)
  }
})

function pin (baton) {
  import('$/io.ox/backbone/views/modal').then(({ default: ModalDialog }) => {
    new ModalDialog({
      async: true,
      point: 'io.ox/guard/files/showPin',
      title: gt('PIN'),
      id: 'showPin',
      width: 300
    })
      .extend({
        content: function () {
          const pin = baton.first().headers['X-OxGuard-PIN']
          this.$body
            .append($('<h2>').append(pin))
        }
      })
      .addButton({ label: gt('OK'), action: 'ok' })
      .on('ok', function () {
        this.close()
      })
      .open()
  })
}

// We need to update the extensions that the email has security json
ext.point('io.ox/mail/detail/attachments').extend({
  index: 1,
  id: 'guardDecrypted',
  draw: function (baton) {
    dupSecurity(baton)
  }
})

ext.point('io.ox/mail/mobile/detail/attachments').extend({
  index: 1,
  id: 'guardDecrypted',
  draw: function (baton) {
    dupSecurity(baton)
  }
})

ext.point('io.ox/mail/externalImages').extend({
  index: 1,
  id: 'checkAuth',
  perform: function (baton) {
    const def = $.Deferred()
    if (baton.data.security && baton.data.security.decrypted) {
      const options = {
        optPrompt: gt('Please re-enter your %s password.', guardModel().getName()),
        minSingleUse: true
      }
      auth_core.authorize(baton, options)
        .done(function () {
          def.resolve()
        })
        .fail(def.reject)
    } else {
      def.resolve()
    }
    return def
  }
})

function dupSecurity (baton) {
  if (baton.data.security && baton.data.attachments.length > 0) {
    for (let i = 0; i < baton.data.attachments.length; i++) {
      baton.data.attachments[i].security = baton.data.security
    }
  }
}

// Function to do authentication validation before calling an extension.
// Checks if valid.  If not, prompts for password, calls extension again once auth
function doAuthCheck (original, baton, storeAuth, minSingleUse) {
  baton.stopPropagation() // Stop this extension and check authorization

  const callback = function (authData) {
    if (baton.data) {
      if (_.isArray(baton.data)) {
        baton.data.forEach(function (d) {
          d.auth = authData
        })
      } else {
        baton.data.auth = authData
      }
    }
    baton.resumePropagation()
    if (storeAuth) {
      if (_.isArray(baton.data)) {
        baton.data.forEach(function (d) {
          d.security = _.extend({ authentication: authData }, d.security)
        })
      } else {
        baton.first().security = _.extend({ authentication: authData }, baton.first().security)
      }
      // Guard-199, direct select of attachment adds list array
      if (baton.list && _.isArray(baton.list)) {
        baton.list.forEach(function (d) {
          d.auth = authData
          d.security = _.extend({ authentication: authData }, d.security)
        })
      }
    }
    original.action(baton)
  }
  const options = {
    optPrompt: gt('Please re-enter your %s password.', guardModel().getName()),
    minSingleUse: minSingleUse !== false,
    callback: callback
  }
  auth_core.authorize(baton, options)
    .fail(function () {
      baton.stopPropagation()
      window.setTimeout(function () {
        baton.resumePropagation()
      }, 500)
    })
}

registerPasswordCheck('io.ox/mail/actions/reply', true)
registerPasswordCheck('io.ox/mail/actions/reply-all', true)
registerPasswordCheck('io.ox/mail/actions/forward', true)
// registerPasswordCheck('io.ox/mail/actions/edit', true)
registerPasswordCheck('io.ox/mail/actions/edit-copy', true)
registerPasswordCheck('io.ox/mail/actions/print')
registerPasswordCheck('io.ox/mail/attachment/actions/view', true, 'default', false)
registerPasswordCheck('io.ox/mail/attachment/actions/open')
registerPasswordCheck('io.ox/mail/attachment/actions/download')
registerPasswordCheck('io.ox/mail/attachment/actions/save')
registerPasswordCheck('io.ox/mail/attachment/actions/vcard')
registerPasswordCheck('io.ox/mail/attachment/actions/ical')

// Function to check actions for Guard emails, and if found, proper authentication
function registerPasswordCheck (extName, storeAuth, id, minSingleUse) {
  ext.point(extName).replace((id || 'default'), function (original) {
    return {
      action: function (baton) {
        if (storeAuth) {
          if (util.isEncryptedMail(baton.first())) {
            baton.first().security = {
              decrypted: true
            }
            doAuthCheck(original, baton, storeAuth, minSingleUse)
            return
          }
        }
        if (!util.isDecrypted(baton.first())) {
          original.action(baton)
          return
        }
        doAuthCheck(original, baton, storeAuth, minSingleUse)
      }
    }
  })
}

/**
     * Extension to check if allowed to forward decrypted/encrypted email
     * Must have Guard-mail capability
     */
ext.point('io.ox/mail/actions/forward').extend({
  id: 'guardLimitedReply',
  index: 10,
  matches: function (baton) {
    if (baton && baton.first()) {
      if (util.isDecrypted(baton.first()) || util.isEncryptedMail(baton.first())) {
        if (!capabilities.has('guard-mail')) {
          baton.stopPropagation()
        }
      }
    }
    return false
  }
})

// Guard Bug-347, double password for composition space
// Comp space emails already have password attached, and Guard will try that first
// Don't prompt for password for spaces.
ext.point('io.ox/mail/actions/edit').replace(('default'), function (original) {
  return {
    action: function (baton) {
      const data = baton.first()
      if (util.isEncryptedMail(data)) {
        baton.first().security = {
          decrypted: true
        }
        const mailref = _.cid({ id: data.id, folder: data.folder_id })
        const space = (ox?.ui?.spaces || {})[mailref]
        if (space) {
          original.action(baton)
        } else {
          doAuthCheck(original, baton, true, true)
        }
        return
      }
      original.action(baton)
    }
  }
})

/// /////// End extensions
/// Functions

// Save public key from attachment
function loadPublicKey (list) {
  _(list).each(function (data) {
    if (testKey(data)) { // make sure key
      let params = '&emailid=' + data.parent.id +
                '&attach=' + data.id +
                '&userid=' + ox.user_id +
                '&cid=' + ox.context_id +
                '&folder=' + data.parent.folder_id
      if (data.security && data.security.decrypted) { // If was encrypted
        import('@/io.ox/guard/auth').then(({ default: auth }) => {
          auth.authorize(list).then(function (auth) {
            params = params + '&auth=' + encodeURIComponent(auth) +
                            '&inline=' + (data.security.pgpInline ? 'true' : 'false') +
                            '&filename=' + encodeURIComponent(data.filename)
            doLoadPublicKey(params)
          })
        })
      } else {
        doLoadPublicKey(params)
      }
    }
  })
}

function doLoadPublicKey (params) {
  const link = ox.apiRoot + '/oxguard/pgpmail/?action=savepublicattach' + params
  http.get(link, '')
    .done(function (data) {
      import('@/io.ox/guard/pgp/keyDetails').then(({ default: keydetails }) => {
        const keys = data.data.externalPublicKeyRings
        let added = gt('Added keys: \r\n')
        for (let i = 0; i < keys.length; i++) {
          added = added + gt('Key ID: ') + keys[i].ids + '\r\n'
          if (keys[i].publicRing && keys[i].publicRing.keys) {
            const details = keydetails.keyDetail(keys[i].publicRing.keys)
            added = added + gt('User IDs: ') + details.keyids
          }
        }
        notify.yell('success', added.replace(/&lt;/g, '<').replace(/&gt;/g, '>'))
      })
    })
    .fail(function (e) {
      console.log(e)
      if (e.status === 503) {
        notify.yell('error', gt('Service Unavailable'))
      } else { notify.yell('error', e.responseText) }
    })
}

export default registerPasswordCheck
