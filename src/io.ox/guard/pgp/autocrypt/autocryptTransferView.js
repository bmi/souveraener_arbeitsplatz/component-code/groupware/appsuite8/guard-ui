/**
 * This work is provided under the terms of the CREATIVE COMMONS PUBLIC
 * LICENSE. This work is protected by copyright and/or other applicable
 * law. Any use of the work other than as authorized under this license
 * or copyright law is prohibited.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 *
 * © 2016 OX Software GmbH, Germany. info@open-xchange.com
 *
 * @author Greg Hill <greg.hill@open-xchange.com>
 */

import Backbone from '$/backbone'
import $ from '$/jquery'
import ext from '$/io.ox/core/extensions'
import ModalView from '$/io.ox/backbone/views/modal'
import yell from '$/io.ox/core/yell'
import gt from 'gettext'
import PasswordView from '@/io.ox/guard/core/passwordView'
import guardModel from '@/io.ox/guard/core/guardModel'
import '@/io.ox/guard/pgp/autocrypt/style.scss'

const POINT = 'oxguard/autoCrypt/transfer'
let INDEX = 0

function open () {
  openModalDialog()
}

function openModalDialog () {
  const dialog = new ModalView({
    async: true,
    point: POINT,
    title: gt('Autocrypt Transfer Keys'),
    id: 'autoCryptStartTransfer',
    width: 640,
    model: new Backbone.Model(),
    enter: 'transfer'
  })
    .addButton({ label: gt('Transfer'), action: 'transfer' })
    .addCancelButton()
    .on('transfer', function () {
      import('@/io.ox/guard/pgp/autocrypt/autoCrypt').then(({ default: autocrypt }) => {
        autocrypt.transferKeys($('#guardpassword').val())
          .then(function (resp) {
            if (resp.data && resp.data.passcode) {
              import('@/io.ox/guard/pgp/autocrypt/autocryptTransferComplete').then(({ default: view }) => {
                view.open(resp.data.passcode)
              })
            }
            if (resp.error) {
              yell('error', resp.error)
              dialog.idle()
              return
            }

            dialog.close()
          })
      })
    })
    .on('cancel', function () {
    })
  dialog.open()
  return dialog
}

ext.point(POINT).extend(
  {
    index: INDEX += 100,
    id: 'detail',
    render: function () {
      const div = $('<div class="autocryptTransfer">')
        .append(gt('This will send an email to you with your current private keys.  The keys will be securely encrypted.  You can then open this email in another AutoCrypt supported email client and import the keys to be used there.'))
      this.$body.append(
        div
      )
    }
  },
  {
    index: INDEX += 100,
    id: 'passwords',
    render: function () {
      const passdiv = $('<div>').addClass('autocryptTransfer')
      const newogpassword = new PasswordView.view({ id: 'guardpassword', class: 'password_prompt form-control', validate: false })
      const label = $('<label for="guardpassword">').append(gt('Please enter your %s password to authorize the transfer.', guardModel().getName()))
      this.$body.append(passdiv.append(label).append(newogpassword.getProtected()))
    }
  })

export default {
  open: open
}
