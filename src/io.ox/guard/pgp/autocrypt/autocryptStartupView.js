/**
 * This work is provided under the terms of the CREATIVE COMMONS PUBLIC
 * LICENSE. This work is protected by copyright and/or other applicable
 * law. Any use of the work other than as authorized under this license
 * or copyright law is prohibited.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 *
 * © 2016 OX Software GmbH, Germany. info@open-xchange.com
 *
 * @author Greg Hill <greg.hill@open-xchange.com>
 */

import Backbone from '$/backbone'
import $ from '$/jquery'
import ox from '$/ox'
import ext from '$/io.ox/core/extensions'
import ModalView from '$/io.ox/backbone/views/modal'
import http from '@/io.ox/guard/core/og_http'
import yell from '$/io.ox/core/yell'
import guardModel from '@/io.ox/guard/core/guardModel'
import PasswordView from '@/io.ox/guard/core/passwordView'
import gt from 'gettext'
import '@/io.ox/guard/pgp/autocrypt/style.scss'

const POINT = 'oxguard/autoCrypt/view'
let INDEX = 0; let lastPass

function open (attachment) {
  load(attachment).then(
    function (details) {
      openModalDialog(attachment, details)
      if (lastPass) {
        window.setTimeout(function () {
          insertData(lastPass.replace(/-/g, ''))
        }, 500)
      }
    }, function (e) {
      yell('error', e)
    })
}

function openModalDialog (attachment, details) {
  const dialog = new ModalView({
    async: true,
    point: POINT,
    title: gt('Autocrypt Start'),
    id: 'autoCryptStart',
    width: 640,
    enter: 'add',
    model: new Backbone.Model({
      attachment: attachment,
      details: details
    })
  })
    .addButton({ label: gt('Add'), action: 'add' })
    .addCancelButton()
    .on('add', function () {
      if (submit(attachment[0])) {
        this.close()
      } else {
        this.idle()
      }
    })
    .on('cancel', function () {
      lastPass = undefined
    })
  load(attachment).then(function () {
    dialog.open()
    window.setTimeout(function () {
      $('#code-0').focus()
    }, 500)
  })
  return dialog
}

function submit (attachment) {
  if ($('#guardpassword').val() !== $('#guardpassword2').val()) {
    return false
  }
  const userdata = {
    password: $('#pgppassword').val(),
    newpassword: $('#guardpassword').val(),
    startkey: getVal()
  }
  const params = '&attachment=' + attachment.id +
        '&id=' + attachment.mail.id + '&folder=' + attachment.mail.folder_id
  http.post(ox.apiRoot + '/oxguard/keys?action=importAutoCryptKeys', params, userdata)
    .done(function (data) {
      if (data && data.data === 'Success') {
        guardModel().clearAuth()
        yell('success', gt('Keys imported successfully'))
        lastPass = undefined
      } else {
        yell('error', data.error ? data.error : gt('Error importing key'))
      }
    })
  return true
}

function inputChanged (e) {
  const input = $(e.currentTarget)
  if (e.keyCode && e.keyCode < 32) { // Check if nav keypress
    return
  }
  if (input.val().length > 4) { // Rare, rapid entry
    input.val(input.val().substring(0, 4))
  }
  if (input.val().length === 4) {
    const inputs = $('.startInput')
    const current = inputs.index(input)
    const next = inputs.eq(current + 1).length ? inputs.eq(current + 1) : $('#pgppassword') // Advance to next or password entry if done
    inputs.eq(current).val(inputs.eq(current).val().toUpperCase())
    next.focus()
  }
}

// Concat all of the input boxes into single recovery string
function getVal () {
  const inputs = $('.startInput')
  let resp = ''
  inputs.each(function (index) {
    resp += $(arguments[1]).val()
    if (index < inputs.length - 1) resp += '-'
  })
  lastPass = resp
  return resp
}

//  Handle user pasting data into input fields
function pasting (e) {
  try {
    let data = e.originalEvent.clipboardData.getData('text')
    data = data.replace(/-/g, '') // remove dashes
    if (data.length > 4) { // If pasting more than just the characters for current box
      e.preventDefault()
      insertData(data)
    }
  } catch (error) {
    console.error(error)
  }
}

// Insert data across all input fields.  Used to either restore failed data, or during paste
function insertData (data) {
  const inputs = $('.startInput')
  let index; let count = 0
  for (index = 0; index < inputs.length; index++) {
    inputs.eq(index).val('') // Clean first
  }
  index = 0
  Array.from(data).forEach(function (c) {
    if (c !== ' ') {
      inputs.eq(index).val(inputs.eq(index).val() + c)
      if (++count === 4) {
        count = 0
        index++
      }
    }
  })
}

ext.point(POINT).extend(
  {
    index: INDEX += 100,
    id: 'detail',
    render: function () {
      const div = $('<div class="autocryptStart">')
        .append(gt('Please enter the setup code to import this key'))
      this.$body.append(
        div
      )
    }
  },
  {
    index: INDEX += 100,
    id: 'input',
    render: function (baton) {
      const chal = baton.model.get('details').data
      const length = parseInt(chal.length, 10)
      const count = length / 4
      // Create a bunch of input boxes, length 4 characters, depending on length of challenge request
      const div = $('<div class="startKeyDiv">')
      for (let i = 0; i < count; i++) {
        const input = $('<input type="text" id="code-' + i + '" class="startInput" aria-label="' + gt('Next 4 characters of the setup key') + '">')
          .keyup(inputChanged)
          .on('paste', pasting)
        if (i === 0 && chal.start) {
          input.val(chal.start)
        }
        div.append(input)
        if (i < count - 1) div.append('<span class="startSpacing">-' + ((i + 1) % 3 === 0 ? '<br>' : '') + '</span>')
      }
      this.$body.append(div)
    }
  },
  {
    index: INDEX += 100,
    id: 'passwords',
    render: function () {
      const passdiv = $('<div>').addClass('row-fluid')
      const newogpassword = new PasswordView.view({ id: 'guardpassword', class: 'password_prompt', validate: true })
      const newogpassword2 = new PasswordView.view({ id: 'guardpassword2', class: 'password_prompt', validate: true })
      const pgpPassword = new PasswordView.view({ id: 'pgppassword', class: 'password_prompt' }).getProtected()
      const hint = $('<td>')

      import('@/io.ox/guard/core/passwords').then(({ default: pass }) => {
        pass.passwordCheck(newogpassword, hint)
        pass.passwordCompare(newogpassword2, newogpassword, hint)
      })
      const table = $('<table class="startupTable">')
      table.append($('<tr>').append($('<td>').append($('<label for="pgppassword">').append(gt('Key password'))))
        .append($('<td>').append(pgpPassword)))
      table.append($('<tr>').append($('<td>').append($('<label for="guardpassword">').append(gt('New password'))))
        .append($('<td>').append(newogpassword.getProtected())))
      table.append($('<tr>').append($('<td>').append($('<label for="guardpassword2">').append(gt('Verify password'))))
        .append($('<td>').append(newogpassword2.getProtected())))
      table.append($('<tr>').append($('<td>')).append($('<td class="hint">').append(hint)))
      this.$body.append(passdiv.append(table))
    }
  })

function load (attachments) {
  const attachment = attachments[0]
  const def = $.Deferred()
  const params = '&attachment=' + attachment.id +
            '&id=' + attachment.mail.id + '&folder=' + attachment.mail.folder_id
  http.get(ox.apiRoot + '/oxguard/keys?action=startAutoCryptImport', params)
    .done(function (data) {
      def.resolve(data)
    })
    .fail(function (err) {
      def.reject()
      console.error(err)
    })
  return def
}

export default {
  open: open
}
