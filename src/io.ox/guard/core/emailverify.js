/**
 * All content on this website (including text, images, source
 * code and any other original works), unless otherwise noted,
 * is licensed under a Creative Commons License.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 *
 *     Copyright (C) 2016-2020 OX Software GmbH
 */

import $ from '$/jquery'
import gt from 'gettext'
import '@/io.ox/guard/core/style.scss'

function validate (email) {
  return (email.match(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[^\s.]*)?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/i))
}

function setValidate (e1, hint) {
  $(e1).on('input', function () {
    if (validate($(e1).val())) {
      $(e1).removeClass('oxguard_badpass')
      if (hint) $(hint).html('')
    } else {
      $(e1).addClass('oxguard_badpass')
      if (hint) {
        $(hint).html(gt('Not valid email'))
      }
    }
  })
}

function autoCompare (e1, e2, hint) {
  $(e2).on('input', function () {
    if ($(e1).val() === $(e2).val()) {
      $(e2).removeClass('oxguard_badpass')
      if (hint) $(hint).html('')
    } else {
      $(e2).addClass('oxguard_badpass')
      if (hint) {
        $(hint).html(gt('Emails not equal'))
      }
    }
  })
}

export default {
  validate: validate,
  autoCompare: autoCompare,
  setValidate: setValidate
}
