/**
 * This work is provided under the terms of the CREATIVE COMMONS PUBLIC
 * LICENSE. This work is protected by copyright and/or other applicable
 * law. Any use of the work other than as authorized under this license
 * or copyright law is prohibited.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 *
 * © 2016 OX Software GmbH, Germany. info@open-xchange.com
 *
 * @author Greg Hill <greg.hill@open-xchange.com>
 */

import $ from '$/jquery'
import _ from '$/underscore'
import Backbone from '$/backbone'
import ext from '$/io.ox/core/extensions'
import ModalView from '$/io.ox/backbone/views/modal'
import PasswordView from '@/io.ox/guard/core/passwordView'
import gt from 'gettext'
import guardModel from '@/io.ox/guard/core/guardModel'
import '@/io.ox/guard/core/style.scss'
import mini from '$/io.ox/backbone/mini-views/common'
import { settings } from '@/io.ox/guard/settings/guardSettings'
import openSettings from '$/io.ox/settings/util'

/// Authentication Section

ext.point('oxguard_core/auth').extend({
  id: 'renderAuth',
  index: 100,
  render: function (baton) {
    // #. %s product name
    let help = $('<label for="ogPassword">' + gt('Please enter your %s password', guardModel().getName()) + '</label>')
    const message = baton.model.get('message')
    if (message !== undefined && message.length > 1) {
      help = $('<label for="ogPassword">' + message + '</label>')
    }
    const passwordBoxView = new PasswordView.view({ id: 'ogPassword', class: 'password_prompt popup_password_prompt', validate: false })
    const pass = passwordBoxView.getProtected().css('display', 'inline')
    this.$body.append(help).append(pass)
    if (baton.model.get('timeOption')) {
      const rememberpass = renderTimeOptions(baton.model, 'popup', gt('Duration'))
      this.$body.append(rememberpass)

      if (guardModel().get('recoveryAvail')) { // Display password help if available
        const forgotDiv = $('<div class="og_forgot">')
        const forgotLink = $('<a href="#">').append(gt('Forgot Password'))
        const dialog = this
        forgotLink.click(function (e) {
          e.preventDefault()
          dialog.close()
          openSettings('virtual/settings/io.ox/guard')
          if (baton.model.get('def')) {
            baton.model.get('def').reject()
          }
        })
        this.$body.append(forgotDiv.append(forgotLink))
      }
    }
  }
})

function renderTimeOptions (model, id, label) {
  const options = [
    {
      value: 10,
      label: gt('10 minutes')
    },
    {
      value: 20,
      label: gt('20 minutes')
    },
    {
      value: 30,
      label: gt('30 minutes')
    },
    {
      value: 60,
      label: gt('1 hour')
    },
    {
      value: 120,
      label: gt('2 hours')
    },
    {
      value: 99999,
      label: gt('Session')
    }
  ]
  const checkbox = new mini.CustomCheckboxView(
    {
      id: 'rememberpass' + id,
      name: 'rememberpass',
      model: model,
      label: gt('Remember my password')
    }).render().$el
  const rememberpass = $('<div>').append(checkbox)
  const defaultDuration = settings.get('defRemember')
  if (defaultDuration) {
    model.set('duration', defaultDuration === '0' ? 99999 : defaultDuration)
    if (defaultDuration !== '0') model.set('rememberpass', true)
  }
  model.on('change:duration', function () {
    model.set('rememberpass', true)
  })
  model.on('change:rememberpass', function () {
    if (model.get('rememberpass')) {
      const val = model.get('duration')
      if (!val) {
        model.set('duration', 99999)
      }
    }
  })
  return $('<div>').addClass('og_duration').append(
    rememberpass,
    $('<label for="duration">').text(label),
    new mini.SelectView({ id: 'duration', name: 'duration', model: model, list: options }).render().$el
  )
}

// Password prompt
function getpassword (message, timeOption, o) {
  const def = $.Deferred()
  if ($('.popup_password_prompt').length > 0) {
    def.reject('duplicate')
    return def // Max one password prompt box open
  }
  const dialog = new ModalView({
    async: true,
    point: 'oxguard_core/auth',
    title: gt('Password needed'),
    width: 350,
    focus: '#ogPassword',
    model: new Backbone.Model({ message: message, timeOption: timeOption, def: def })
  })
    .build(function () {
    })
    .addButton({ label: 'OK', action: 'ok' })
    .addCloseButton()
    .on('cancel', function () {
      def.reject('cancel')
    })
    .on('ok', function () {
      go(def, o, this.model)
      this.close()
    })
  // Custom action for enter here.  Focus the enter button, then allow keypress to continue
  // Allows onclick function for the button, allowing popups
  dialog.$el.on('keydown', function (e) {
    if (e.which !== 13) return
    if (!$(e.target).is('input:text, input:password')) return
    $('.modal-footer [data-action="ok"]').focus()
  })
  dialog.open()
  return (def)
}

function go (def, o, model) {
  let duration = -1
  if (model.get('rememberpass')) {
    duration = model.get('duration')
  }
  const data = {
    password: $('#ogPassword').val(),
    duration: duration
  }
  if (o && _.isFunction(o.preAuthCallback)) {
    o.preAuthCallback()
  }
  def.resolve(data)
}

function open (message, timeOption, o) {
  return getpassword(message, timeOption, o)
}

export default {
  open: open,
  renderTimeOptions: renderTimeOptions
}
