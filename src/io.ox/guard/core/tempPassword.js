/**
 * All content on this website (including text, images, source
 * code and any other original works), unless otherwise noted,
 * is licensed under a Creative Commons License.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 *
 * Copyright (C) 2016-2020 OX Software GmbH
 */

import $ from '$/jquery'
import _ from '$/underscore'
import ox from '$/ox'
import ModalDialog from '$/io.ox/backbone/views/modal'
import authAPI from '@/io.ox/guard/api/auth'
import PasswordView from '@/io.ox/guard/core/passwordView'
import loginAPI from '@/io.ox/guard/api/login'
import gt from 'gettext'
import guardModel from '@/io.ox/guard/core/guardModel'
import { settings as mailSettings } from '$/io.ox/mail/settings'

let firstprompt = false

function createOxGuardPasswordPrompt (baton, go, errors, oldpass, index, priorData) {
  if (firstprompt) return
  firstprompt = true

  const dialog = new ModalDialog({
    async: true,
    point: 'io.ox/guard/core/tempPassword',
    id: 'tempPassword',
    width: 560,
    title: gt('First %s Security Use', guardModel().getName()),
    data: {
      isnew: guardModel().get('new'),
      hasPin: guardModel().get('pin'),
      baton: baton,
      go: go,
      errors: errors,
      oldpass: oldpass,
      index: index,
      priorData: priorData
    }
  })
    .extend({
      header: function () {
        const explain = $('<div>')
        const data = this.options.data
        if (data.isnew) {
          // #. %s product name
          explain.append($('<p>').append(gt('You have been sent an email using %s to secure the contents', guardModel().getName())))
          explain.append($('<p>').append(gt('Please select a new password for use with %s.  This password will be used to open encrypted emails and files that may be sent to you.', guardModel().getName())))
        } else {
          // #. %s product name
          explain.append($('<p>').append(gt('Please change the initial temporary %s security password that was sent to you in a separate email.', guardModel().getName())))
        }
        this.$body.append(explain)
      },
      passDiv: function () {
        const passdiv = $('<div>').addClass('row-fluid')
        const newogpassword = new PasswordView.view({ id: 'newogpassword', class: 'password_prompt', validate: true })
        const td1 = $('<td>').append(newogpassword.getProtected())
        const newogpassword2 = new PasswordView.view({ id: 'newogpassword2', class: 'password_prompt' })
        const td2 = $('<td>').append(newogpassword2.getProtected())
        const hint = $('<td>')
        const table = $('<table class="og_password_prompt"/>')
        const data = this.options.data
        if (!data.isnew) {
          const oldPassword = new PasswordView.view({ id: 'oldogpassword', class: 'password_prompt', validate: false })
          table
            .append($('<tr>')
              .append($('<td class="pw">').append($('<label for="oldogpassword">').append(gt('Initial Password'))))
              .append($('<td>').append(oldPassword.getProtected())))
        }
        table
          .append($('<tr><td class="pw"><label for="newogpassword">' + gt('New Password') + ':</label></td></tr>').append(td1).append(hint))
          // #. Confirm new password
          .append($('<tr><td class="pw"><label for="newogpassword2">' + gt('Confirm New') + ':</label></td></tr>').append(td2).append('<td></td>'))
        passdiv.append(table)
        this.$body.append(passdiv)

        import('@/io.ox/guard/core/passwords').then(({ default: pass }) => {
          pass.passwordCheck(newogpassword, hint)
          pass.passwordCompare(newogpassword2, newogpassword, hint)
        })
      },
      pin: function () {
        const data = this.options.data
        if (data.hasPin) {
          const pinPrompt = $('<p>').append(gt('The sender assigned an additional PIN to the account.  This PIN must be entered before you can use this account.  Please enter it now.  Contact the sender directly if you don\'t yet have a pin.'))
          const pinTable = $('<table class="og_password_prompt">')
          const pinLabel = $('<td class="pw">').append('<label for="pinPrompt">' + gt('PIN') + ':</label>')
          const pinEntry = $('<input id="pinInput" name="pinInput" class="form-control password_prompt">')
          const that = this
          pinEntry.focus(function () {
            that.$body.animate({ scrollTop: $('#pinInput').offset().top })
          })
          this.$body.append('<hr/>').append(pinPrompt).append(pinTable.append($('<tr>').append(pinLabel).append($('<td>').append(pinEntry))))
        }
      },
      recovery: function () {
        if (guardModel().getSettings().noRecovery !== true) {
          const rule = $('<hr/>')
          const recovery = $('<p>' + gt('Please enter a secondary email address in case you need to reset your password.' + '</p>'))

          const table2 = $('<table class="og_password_prompt"/>')
          const email1 = $('<input id="recoveryemail" name="recoveryemail" class="form-control password_prompt"/>')
          const email2 = $('<input id="verifyemail" name="verifyemail" class="form-control password_prompt"/>')
          const hint2 = $('<td>')
          const row = $('<tr>').append($('<td class="pw"><label for="recoveryemail">' + gt('Email:') + '</label></td>')).append($('<td>').append(email1)).append(hint2)
          const row2 = $('<tr>').append($('<td class="pw"><label for="verifyemail">' + gt('Verify:') + '</label></td>')).append($('<td>').append(email2))
          table2.append(row).append(row2)
          const that = this
          email1.focus(function () {
            that.$body.animate({ scrollTop: $('#recoveryemail').offset().top })
          })
          this.$body.append(rule).append(recovery).append(table2)
          import('@/io.ox/guard/core/emailverify').then(({ default: verify }) => {
            verify.setValidate(email1, hint2)
            verify.autoCompare(email1, email2, hint2)
          })
        } else {
          const warning = $('<p class="recoveryWarning">').append($('<b>').append(gt('Warning: This password for encryption cannot be restored or recovered in any way.  If forgotten, all encrypted data will be lost.')))
          this.$body.append($('<hr/>')).append(warning)
        }
      },
      errors: function () {
        const data = this.options.data
        if (data.errors) {
          this.$body.append($('<div class="alert-danger error-line">').append($('<span>').append(data.errors).attr('role', 'alert')))
        }
      }
    })
    .addButton({ label: gt('OK'), action: 'okpass' })
    .addCancelButton()
    .on('cancel', function () {
      firstprompt = false
    })
    .on('okpass', function () {
      ox.busy()
      const data = this.options.data
      doChange(data.hasPin, data.baton, data.go, data.index).then(function () {
        ox.idle()
        dialog.close()
      })
    })
    .on('open', function () {
      const data = this.options.data
      const priorData = data.priorData
      if (priorData) { // If error dialog, and reloaded, bring back old data
        $('#newogpassword').val(priorData.newpass)
        $('#newogpassword2').val(priorData.newpass)
        $('input[name="recoveryemail"]').val(priorData.email)
        $('input[name="verifyemail"]').val(priorData.email)
      }
      const oldpass = data.oldpass
      if (oldpass !== undefined) {
        $('#oldogpassword').val(oldpass)
        if (!_.device('ios')) {
          $('#newogpassword').focus()
        }
      } else
      if (!_.device('ios')) {
        $('#oldogpassword').focus()
      }
      const that = this
      window.setTimeout(function () {
        if (data.errors) {
          if ($('#recoveryemail').length > 0) {
            that.$body.animate({ scrollTop: $('#recoveryemail').offset().top })
          } else {
            that.$body.animate({ scrollTop: $('.recoveryWarning').offset().top })
          }
        }
        let j = 0
        $('.pw').each(function (i, v) {
          const w = $(v).width()
          if (w > j) j = w
        })
        $('.pw').each(function (i, v) {
          $(v).width(j)
        })
        const email = $('input[name="recoveryemail"]') // Watch email entry for valid
        email.on('keyup', function () {
          if (validateEmail(email.val())) {
            email.css('background-color', 'white')
          } else {
            email.css('background-color', 'rgb(253, 187, 179)')
          }
        })
      }, 0)
    })
    .open()
}

async function doChange (hasPin, baton, go, index) {
  firstprompt = false
  const oldpass = $('#oldogpassword').val() === undefined ? '' : $('#oldogpassword').val()
  const pass1 = $('#newogpassword').val()
  const pass2 = $('#newogpassword2').val()
  const pin = hasPin ? $('#pinInput').val() : ''
  const emailaddr = $('input[name="recoveryemail"]').val() === undefined ? '' : $('input[name="recoveryemail"]').val()
  const verify = $('input[name="verifyemail"]').val() === undefined ? '' : $('input[name="verifyemail"]').val()
  if (emailaddr !== verify) {
    $('input[name="verifyemail"]').css('background-color', 'salmon')
    createOxGuardPasswordPrompt(baton, go, gt('Emails not equal'), oldpass)
    return
  }
  if (pass1 !== pass2) {
    $('input[name="newogpassword2"]').css('background-color', 'salmon')
    createOxGuardPasswordPrompt(baton, go, gt('Passwords not equal'), oldpass)
    return
  }
  const defaultSendAddress = $.trim(mailSettings.get('defaultSendAddress', ''))

  const userdata = {
    newpass: pass1,
    oldpass: oldpass,
    email: emailaddr,
    userEmail: defaultSendAddress || ox.user,
    user_id: ox.user_id,
    sessionID: ox.session,
    cid: ox.context_id,
    pin: pin
  }
  if ((emailaddr.length > 1) && (!validateEmail(emailaddr))) {
    createOxGuardPasswordPrompt(baton, go, gt('Enter new secondary Email address'), oldpass)
    return
  }
  if (validateEmail(emailaddr)) userdata.email = emailaddr
  import('$/io.ox/core/notifications').then(({ default: notifications }) => {
    loginAPI.changePassword(userdata)
      .done(function (data) {
        if (typeof data === 'string') data = $.parseJSON(data)
        if (data.auth === undefined) {
          if (data.error) {
            createOxGuardPasswordPrompt(baton, go, data.error, oldpass, index, userdata)
          } else {
            import('@/io.ox/guard/core/errorHandler').then(({ default: err }) => {
              err.showError(data)
            })
          }
          return
        }
        if (data.auth.length > 20) {
          $('#grdsettingerror').text(gt('Success'))
          $('input[name="newogpassword"]').val('')
          $('input[name="newogpassword2"]').val('')
          guardModel().clearAuth()
          guardModel().set('recoveryAvail', !guardModel().getSettings().noRecovery)
          authAPI.setToken(data.auth).then(function () {
            if (go) go(baton, pass1, index)
          })
          notifications.yell('success', gt('Password changed successfully'))
          guardModel().set('pin', false)
          updateSecondary()
          guardModel().set('new', false)
          // Complete, now offer tour
          Promise.all([import('@/io.ox/guard/core/createKeys'),
            import('$/io.ox/core/capabilities')])
            .then(function ([{ default: keys }, { default: capabilities }]) {
              if (capabilities.has('guard-mail') && !capabilities.has('guest')) { // only run tour if has guard-mail
                keys.createKeysWizard()
              }
            })
        } else {
          if (data.auth === 'Bad new password') {
            notifications.yell('error', gt('New password must be at least %s characters long', data.minlength))
            return
          }
          if (data.auth === 'Bad password') {
            // #. Bad, incorrect password
            notifications.yell('error', gt('Bad password'))
            return
          }
          if (data.auth === 'Key not found') {
            notifications.yell('error', gt('Encryption key not found'))
            return
          }
          notifications.yell('error', gt('Failed to change password'))
        }
      })
      .fail(function (data) {
        if (data.error) {
          createOxGuardPasswordPrompt(baton, go, data.error, oldpass, index, userdata)
        } else {
          notifications.yell('error', gt('Failed to change password'))
        }
      })
  })
}

// Valide legit email address
function validateEmail (email) {
  const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  return re.test(email)
}

function updateSecondary () {
  if ($('#currentsecondary').length > 0) {
    import('@/io.ox/guard/settings/pane').then(({ default: settingsPage }) => {
      settingsPage.updateEmail()
    })
  }
}

export default {
  createOxGuardPasswordPrompt: createOxGuardPasswordPrompt
}
