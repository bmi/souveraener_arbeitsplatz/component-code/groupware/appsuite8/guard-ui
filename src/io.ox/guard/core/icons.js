/**
 * All content on this website (including text, images, source
 * code and any other original works), unless otherwise noted,
 * is licensed under a Creative Commons License.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 *
 * Copyright (C) 2016-2020 OX Software GmbH
 */

// Central location for icons

import { createIcon } from '$/io.ox/core/components'
import _ from 'underscore'

const lock = '<svg version="1.2" baseProfile="tiny" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"x="0px" y="0px" viewBox="0 0 16 16" overflow="visible" class="bi bi-lock" xml:space="preserve"><path d="M8,1c1.1,0,2,0.9,2,2v4H6V3C6,1.9,6.9,1,8,1z M11,7V3c0-1.7-1.3-3-3-3S5,1.3,5,3v4C3.9,7,3,7.9,3,9v5c0,1.1,0.9,2,2,2h6c1.1,0,2-0.9,2-2V9C13,7.9,12.1,7,11,7z M5,8h6c0.6,0,1,0.4,1,1v5c0,0.6-0.4,1-1,1H5c-0.6,0-1-0.4-1-1V9C4,8.4,4.4,8,5,8z"/><circle cx="8" cy="10.3" r="1"/><rect x="7.5" y="10.9" width="1" height="2.8"/></svg>' // eslint-disable-line no-tabs
const unlock = '<svg version="1.2" baseProfile="tiny" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"x="0px" y="0px" viewBox="0 0 16 16" overflow="visible" class="bi bi-unlock" xml:space="preserve"><path d="M11,1C9.9,1,9,1.9,9,3v4c1.1,0,2,0.9,2,2v5c0,1.1-0.9,2-2,2H3c-1.1,0-2-0.9-2-2V9c0-1.1,0.9-2,2-2h5V3c0-1.7,1.3-3,3-3s3,1.3,3,3v4c0,0.3-0.2,0.5-0.5,0.5S13,7.3,13,7V3C13,1.9,12.1,1,11,1z M3,8C2.4,8,2,8.4,2,9v5c0,0.6,0.4,1,1,1h6c0.6,0,1-0.4,1-1V9c0-0.6-0.4-1-1-1H3z"/><circle cx="6" cy="10.4" r="1"/><rect x="5.5" y="11.1" width="1" height="2.8"/></svg>' // eslint-disable-line no-tabs
function addClass (iconRaw, name, className) {
  const icon = createIcon(iconRaw)
  return icon.addClass([className])
}

const icons = {
  getLocked: function (className) {
    return addClass(lock, 'lock', className)
  },
  getUnLocked: function (className) {
    return addClass(unlock, 'un-lock', className)
  },
  getIcon: function (opt) {
    const iconHolder =
      _.isString(opt) ? createIcon(opt) : createIcon('bi/' + opt.name + '.svg', opt)
    return iconHolder
  }
}

export default icons
