/**
 * All content on this website (including text, images, source
 * code and any other original works), unless otherwise noted,
 * is licensed under a Creative Commons License.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 *
 * Copyright (C) 2016-2020 OX Software GmbH
 */

import capabilities from '$/io.ox/core/capabilities'
import ox from '$/ox'
import { settings } from '@/io.ox/guard/settings/guardSettings'
import Backbone from '$/backbone'
import _ from '$/underscore'

let guardModel // instance of Guard Model

function sanitize (data) {
  const regex = new RegExp('<(?:!--(?:(?:-*[^->])*--+|-?)|script\\b(?:[^"\'>]|"[^"]*"|\'[^\']*\')*>[\\s\\S]*?</script\\s*|style\\b(?:[^"\'>]|"[^"]*"|\'[^\']*\')*>[\\s\\S]*?</style\\s*|/?[a-z](?:[^"\'>]|"[^"]*"|\'[^\']*\')*)>', 'gi')
  return (data.replace(regex, ''))
}

// Parse out available languages, comparing Guard templates and UI available
function checkLanguages (guardLanguages) {
  const available = {}
  if (!guardLanguages) return available
  for (const lang in guardLanguages) {
    if (ox.serverConfig.languages[lang]) {
      available[lang] = guardLanguages[lang]
    }
  }
  return available
}

const GuardModel = Backbone.Model.extend({

  defaults: {
    authcode: null,
    settings: {},
    loaded: false
  },

  initialize: function () {
    this.on('change:loaded', this.loadEvents)
  },
  /** Initialize with the Guard login data */
  loadData: function (data) {
    const model = this
    Object.entries(data).forEach(function ([key, value]) {
      switch (key) {
        case 'lang':
          model.set('lang', checkLanguages(value))
          break
        case 'auth':
          break
        default:
          model.set(key, value)
      }
    })
    this.set('loaded', true)
  },

  loadEvents: function () { // Load any functions pending Guard loaded
    const startupList = this.get('onload')
    if (startupList) {
      try {
        startupList.forEach(function (load) {
          load.call()
        })
        this.set('onload', null)
      } catch (e) {
        console.error('Problem executing onload functions for Guard ' + e)
      }
    }
  },

  getName: _.memoize(function () {
    const name = ox.serverConfig['guard.productName'] || settings.get('productName')
    return sanitize((!name || name === 'undefined') ? 'Guard' : name)
  }),

  // ****** Authentication  */
  setAuth: function (auth) {
    switch (auth) {
      case 'Bad Password':
        return
      case 'Password Needed':
        this.set('needsPassword', true)
        return
      case 'No Key':
        this.set('needsKey', true)
        return
      default:
        this.set('authcode', auth)
        this.set('needsPassword', false)
        this.set('needsKey', false)
    }
  },

  clearAuth: function () {
    this.setAuth(null)
  },

  hasAuth: function () {
    return this.getAuth() !== null
  },

  getAuth: function () {
    return this.get('authcode')
  },

  /** Guard key status  */
  needsPassword: function () {
    return this.get('needsPassword') === true
  },

  needsKey: function () {
    return this.get('needsKey') === true
  },

  notReady: function () {
    return this.needsPassword() || this.needsKey()
  },

  /* *Guard status **/
  isLoaded: function () {
    return this.get('loaded')
  },

  hasGuardMail: function () {
    return this.getSettings().oxguard && capabilities.has('guard-mail')
  },

  getSettings: function () {
    return this.get('settings') || {}
  }
})

export default function getModel () {
  if (!guardModel) {
    return initialize()
  }
  return guardModel
}

export function initialize (data) {
  data = data || {
    passcode: null,
    settings: {}
  }
  guardModel = new GuardModel(data)
  return guardModel
}
