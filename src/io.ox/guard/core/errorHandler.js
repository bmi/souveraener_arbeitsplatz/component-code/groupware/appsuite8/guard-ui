/**
 * All content on this website (including text, images, source
 * code and any other original works), unless otherwise noted,
 * is licensed under a Creative Commons License.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 *
 * Copyright (C) 2016-2020 OX Software GmbH
 */

import notify from '$/io.ox/core/notifications'

function showError (json) {
  if (json[0] !== undefined) json = json[0]
  if (json.error) {
    notify.yell('error', json.error)
  }
}

export default {
  showError: showError
}
