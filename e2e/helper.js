const Helper = require('@open-xchange/codecept-helper').helper
const output = require('codeceptjs').output
const codecept = require('codeceptjs')
const config = codecept.config.get()

class GuardHelper extends Helper {
  amDisabled (filter) {
    if (!Array.isArray(filter)) {
      filter = [filter]
    }
    let disabled = false
    const ox = config.helpers.OpenXchange
    if (ox.skipTests) {
      const list = ox.skipTests.toLowerCase()
      filter.forEach(d => {
        if (list.indexOf(d.toLowerCase()) > -1) {
          disabled = true
          output.log('Disabling test. Skip items: ' + d)
        }
      })
    }
    return disabled
  }

  async verifyUserSetup (o) {
    const user = o.user.userdata
    const { page } = this.helpers.Puppeteer

    assert(user && user.password, 'user or user password not configured') // Check user exists

    const result = await page.evaluate(async function (user) {
      return new Promise(function (resolve, reject) {
        import('./io.ox/guard/core/guardModel.js').then(({ default: guardModel }) => {
          if (guardModel().needsKey()) { // Needs setup
            return import('./io.ox/guard/api/keys.js').then(({ default: api }) => {
              return api.create({ name: user.name, email: user.email1, password: user.password }).then((data) => {
                if (data.error) reject(data.error_desc)
                if (data.publicRing) {
                  guardModel().setAuth(null)
                  guardModel().set('recoveryAvail', !guardModel().getSettings().noRecovery)
                  resolve(true)
                }
              })
            })
          } else {
            reject(new Error('fail')) // temporary password or lockout
          }
        })
      })
    }, user)

    assert(result === true, result)
  }

  insertPassword (selector, o) {
    const user = o.user.userdata
    if (!user.password) this.assert.fail('user without password', 'Expected a user with password when calling insertPassword.')
    this.clearValue(selector)
    this.setValue(selector, user.password)

    return this
  }

  async downloadPrivateKey (keyId, o) {
    const userdata = o.user.userdata
    const { page } = this.helpers.Puppeteer
    const result = await page.evaluate(function (userdata, id) {
      return new Promise(function (resolve, reject) {
        Promise.all([import('./jquery.js'), import('./underscore.js'),
          import('./ox.js'), import('./io.ox/guard/core/og_http.js')])
          .then(function ([{ default: $ }, { default: _ }, { default: ox }, { default: og_http }]) {
            const params = '&keyid=' + (_.isArray(id) ? id[0] : id) +
                '&keyType=private'
            const data = {
              password: userdata.password
            }
            const link = ox.apiRoot + '/oxguard/keys?action=downloadKey'
            og_http.simplePost(link, params, data).then(resolve, reject)
          })
      })
    }, userdata, keyId)
    return result
  }

  async addToTextDocument (textToAdd) {
    const { page } = this.helpers.Puppeteer
    page.keyboard.type(textToAdd)
  }

  async selectEmailNumber (num) {
    const { page } = this.helpers.Puppeteer
    const selector = 'ul.list-view li:nth-child(' + (num + 1) + ')'
    await page.$eval(selector, el => { el.click() })
  }

  getThreadSelector (num) {
    return '.thread-view article:nth-of-type(' + num + ')'
  }

  async openThreadEmail (num) {
    const { page } = this.helpers.Puppeteer
    const selector = this.getThreadSelector(num)
    return new Promise(function (resolve, reject) {
      page.evaluate(async function (num, selector) {
        const { default: $ } = await import('./jquery.js')
        const def = $.Deferred()
        function isOpen () {
          return $(selector + ' .content').is(':visible') || $(selector + ' .body').is(':visible')
        }
        function tryOpen (attempts) {
          console.log('attempting' + attempts)
          console.log(selector)
          console.log(selector + ' .toggle-mail-body')
          $(selector + ' .toggle-mail-body').click()
          setTimeout(function () {
            if (isOpen()) {
              def.resolve()
              return
            }
            if (!isOpen() && attempts++ < 2) {
              tryOpen(attempts)
            }
            if (attempts > 2) {
              def.reject()
            }
          }, 500)
        }
        if (isOpen()) {
          def.resolve()
        } else {
          tryOpen(0)
        }
        return def
      }, num, selector).then(resolve, reject)
    })
  }
}
module.exports = GuardHelper
