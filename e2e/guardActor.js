const actor = require('@open-xchange/codecept-helper').actor
const _ = require('underscore')
module.exports = function () {
  return _.extend(actor(), {
    insertCryptPassword: require('./commands/insertCryptPassword'),
    auth: require('./commands/auth'),
    wipeUser: require('./commands/wipeUser'),
    wipeInbox: require('./commands/wipeInbox'),
    sendEmail: require('./commands/sendEmail'),
    sendMobileEmail: require('./commands/sendMobileEmail'),
    sendEmailGuest: require('./commands/sendEmailGuest'),
    verifyDecryptedMail: require('./commands/verifyDecryptedMail'),
    verifyDecryptedGuestMail: require('./commands/verifyDecryptedGuestMail'),
    changeTemporaryPassword: require('./commands/changeTemporaryPassword'),
    clickGuestLink: require('./commands/clickGuestLink'),
    setupUser: require('./commands/setupUser'),
    startEncryptedEmail: require('./commands/startEncryptedEmail')
  })
}
