Running Guard E2E tests

Guard tests will require a working Appsuite installation, including Documents, and Document-converter.  OX Soap interface will be used to create and remove both users and contexts.  A supporting dovecot environment should be set up to provide imap for these created users (often easiest to just use master password setup)

The following Guard settings will be required

- com.openexchange.guard.pinEnabled=true

Sharing must be set up and working.  The share links must be accessible.

For mailfilter tests, the MW must have mailfiltering setup.  See https://oxpedia.org/wiki/index.php?title=AppSuite:OX_Guard_MailFilter

Edit or create an .env file with the required settings for your setup.  And example file .env-example has been provided.

The HEADLESS setting in the .env file will establish if chrome is opened or if it is run headless.  Running headless is faster.

To run tests, execute:
yarn e2e

To limit which tests are used, you can apply a grep function against the names, such as:
yarn e2e --grep "Autocrypt"



