/**
 * This work is provided under the terms of the CREATIVE COMMONS PUBLIC
 * LICENSE. This work is protected by copyright and/or other applicable
 * law. Any use of the work other than as authorized under this license
 * or copyright law is prohibited.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 * © 2017 OX Software GmbH, Germany. info@open-xchange.com
 *
 * @author Greg Hill <greg.hill@open-xchange.com>
 */

/**
 * This sets up the settings for a user
 */

module.exports  = async function (o, advanced) {

    //await this.haveSetting({ 'io.ox/tours': { 'server/disableTours': true, 'whatsNew/neverShowAgain': true }}, { user: o.user, noCache: true });
    if (advanced) {
        await this.haveSetting('oxguard//advanced', true, { user: o.user, noCache: true });
    }
}