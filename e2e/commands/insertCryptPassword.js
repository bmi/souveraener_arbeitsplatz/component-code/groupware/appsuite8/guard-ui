/**
 * This work is provided under the terms of the CREATIVE COMMONS PUBLIC
 * LICENSE. This work is protected by copyright and/or other applicable
 * law. Any use of the work other than as authorized under this license
 * or copyright law is prohibited.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 * © 2017 OX Software GmbH, Germany. info@open-xchange.com
 *
 * @author Greg Hill <greg.hill@open-xchange.com>
 */

/**
 * This simplifies the input of passwords into input fields.
 * There is an array of users defined in the globals. If you want to fill in a password of such a user
 * you can simply use this function.
 * @param selector {string} the selector of an editable field
 * @param userIndex {number} the users position in the global users array
 */
module.exports = function (selector, o) {
  const user = o.user.userdata
  this.clearField(selector)
  this.fillField(selector, user.password)

  return this
}
