/**
 * This work is provided under the terms of the CREATIVE COMMONS PUBLIC
 * LICENSE. This work is protected by copyright and/or other applicable
 * law. Any use of the work other than as authorized under this license
 * or copyright law is prohibited.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 * © 2017 OX Software GmbH, Germany. info@open-xchange.com
 *
 * @author Greg Hill <greg.hill@open-xchange.com>
 */

/**
 * This sends a new email to recipient
 */

const SELECTOR = require('../constants')

module.exports = function (o, subject, data, pin, options) {
  const userdata = o.user.userdata
  options = options || {}
  const optionsDropdown = locate({ css: SELECTOR.MAIL_OPTIONS }).as('Options dropdown')

  // Open compose
  this.wait(2)
  this.click('New email')
  this.waitForVisible(SELECTOR.WINDOW_BUSY)
  this.waitForInvisible(SELECTOR.WINDOW_BUSY)
  this.waitForVisible(SELECTOR.EDITOR)

  // Mark as secure
  this.click('.toggle-encryption')
  this.waitForVisible('#ogPassword')
  this.fillField('#ogPassword', userdata.password)
  this.click('.btn[data-action="ok"]')
  this.waitForVisible(SELECTOR.GUARD_LOCK)

  // Verify HTML
  this.click(optionsDropdown)
  this.click('HTML')
  this.waitForVisible('.io-ox-mail-compose .editor .tox-edit-area')

  // Insert test data
  const I = this
  let recipLocation = SELECTOR.TO_FIELD
  if (options.type) {
    if (options.type === 'cc') {
      I.click('CC')
      I.waitForVisible('[placeholder="CC"]')
      recipLocation = SELECTOR.CC
    }
    if (options.type === 'bcc') {
      I.click('BCC')
      I.waitForVisible('[placeholder="BCC"]')
      recipLocation = SELECTOR.BCC
    }
  }
  this.fillField(recipLocation, userdata.email1)
  this.fillField(SELECTOR.SUBJECT, subject)

  within({ frame: SELECTOR.EDITOR_IFRAME }, async () => {
    I.fillField(SELECTOR.EMAIL_BODY, data)
  })

  // Verify icon is next to recipient names
  this.waitForVisible(SELECTOR.GUEST_ICON, 10)
  this.moveCursorTo(SELECTOR.GUEST_ICON, 2, 2)
  // this.waitForVisible('.tooltip');
  // this.see('guest', '.tooltip');

  // Send
  this.click('Send')

  // Add guest message
  this.waitForVisible('#message')
  this.see('Recipient Options')
  this.fillField('.og_guest_message', 'Test introduction text')

  this.click('OK')

  if (!pin) { // Additional handling required for pin
    this.waitForVisible('.io-ox-mail-window .leftside')
    I.wait(1)
    I.waitForInvisible('.mail-progress')
  }
}
