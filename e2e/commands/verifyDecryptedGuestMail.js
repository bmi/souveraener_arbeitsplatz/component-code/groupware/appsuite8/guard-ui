/**
 * This work is provided under the terms of the CREATIVE COMMONS PUBLIC
 * LICENSE. This work is protected by copyright and/or other applicable
 * law. Any use of the work other than as authorized under this license
 * or copyright law is prohibited.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 * © 2017 OX Software GmbH, Germany. info@open-xchange.com
 *
 * @author Greg Hill <greg.hill@open-xchange.com>
 */

/**
 * Verifies the decrypted email data is as expected
 * 
 */


module.exports  = function (subject, data) {
    this.waitForVisible('.thread-view-header .subject');
    this.see(subject);
    this.waitForVisible('.oxguard_icon_fa');  // Verify PGP icon
    this.switchTo('.mail-detail-frame');
    this.see(data);  // Check data in body
    this.switchTo();
};