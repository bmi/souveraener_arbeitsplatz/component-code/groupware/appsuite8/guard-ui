/**
 * This work is provided under the terms of the CREATIVE COMMONS PUBLIC
 * LICENSE. This work is protected by copyright and/or other applicable
 * law. Any use of the work other than as authorized under this license
 * or copyright law is prohibited.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 * © 2017 OX Software GmbH, Germany. info@open-xchange.com
 *
 * @author Greg Hill <greg.hill@open-xchange.com>
 */

/// <reference path="../../steps.d.ts" />

const SELECTOR = require('../../constants.js')

Feature('Settings')

Before(async function ({ users }) {
  await users.create()
})

After(async function ({ users }) {
  await users.removeAll()
})

Scenario('First Setup and tour', async function ({ I, users, settings }) {
  const o = {
    user: users[0]
  }

  await I.setupUser(o)

  // Next, log in to settings
  I.login(['app=io.ox/settings', 'folder=virtual/settings/io.ox/guard'], o)

  I.waitForElement('.io-ox-guard-settings')
  I.waitForElement('.guard-start-button')
  I.wait(1) // Let background loading finish
  // Click the setup button and verify wizard;
  I.click('.guard-start-button')
  I.waitForElement(SELECTOR.WIZARD_NEXT)
  I.see('Start Setup')

  // Click Start
  I.click(SELECTOR.WIZARD_NEXT)
  I.waitForElement('.wizard-content .guard-create-key')
  I.see('Please enter a password')

  // Try mismatched password and verify error
  I.insertCryptPassword('#newogpassword', o)
  I.wait(1)
  I.fillField('#newogpassword2', 'mismatch')
  I.wait(1)
  I.see('Passwords not equal')
  I.seeElement(SELECTOR.WIZARD_NEXT + ':disabled')

  // Correct password and verify error gone
  I.insertCryptPassword('#newogpassword2', o)
  I.wait(1)
  // Verify next button no longer disabled
  I.seeElement(SELECTOR.WIZARD_NEXT + ':not(:disabled)')
  I.fillField('input[name="recoverymail"]', 'test@email.com')
  I.wait(1)
  I.click(SELECTOR.WIZARD_NEXT)

  // Verify complete shown, and start tour
  I.waitForElement('#startTourLink')
  I.see('Guided tour')
  I.click(SELECTOR.DONE_BUTTON)

  // Make sure tour is set for non-advanced
  I.executeScript(async function () {
    const { default: $ } = await import('./jquery.js')
    if ($('[name="advanced"]').prop('checked')) { $('[name="advanced"]').click() }
  })

  I.wait(1)
  settings.close()
  I.click(SELECTOR.TOPBAR_HELP)
  I.click('a[data-name="GuardTour"]')
  I.waitForVisible(SELECTOR.WINDOW_BUSY)
  I.waitForInvisible(SELECTOR.WINDOW_BUSY)
  I.waitForElement('.contenteditable-editor')
  I.waitForElement('.hotspot')
  I.waitForElement('.wizard-content')
  I.see('Security and privacy')
  I.click(SELECTOR.WIZARD_NEXT)

  // Verify Seucrity Settings opened, and password management illustrated
  I.waitForElement('.io-ox-guard-settings')
  I.waitForElement('.wizard-content')
  I.see('You already set up')
  I.waitForElement(SELECTOR.SPOTLIGHT) // Highlighted area
  I.click(SELECTOR.WIZARD_NEXT)

  // Begin illustrating Defaults
  I.waitForElement('.hotspot')
  I.waitForElement(SELECTOR.SPOTLIGHT)
  I.see('From here')
  I.click(SELECTOR.WIZARD_NEXT)

  // Default signing
  I.waitForElement('.hotspot')
  I.see('In addition to encrypting')
  I.click(SELECTOR.WIZARD_NEXT)

  // Advanced options
  I.waitForElement('.hotspot')
  I.see('Advanced User Settings')
  I.click(SELECTOR.WIZARD_NEXT)

  // Opens up compose again to show unlock
  I.waitForElement(SELECTOR.EDITOR)
  I.waitForElement('.hotspot')
  I.waitForElement('.wizard-content')
  I.see('Enabling and disabling')
  I.click(SELECTOR.WIZARD_NEXT)

  // Show file encryption
  I.waitForElement('.io-ox-files-main .window-sidepanel')
  I.waitForElement('.wizard-title')
  I.see('Encrypt files')
  I.click(SELECTOR.WIZARD_NEXT)

  // Show option to restart tour
  I.waitForElement('[data-name="GuardTour"]')
  I.waitForElement('.hotspot')
  I.see('Restart Guided Tour')
  I.click(SELECTOR.WIZARD_NEXT)

  // Show done
  I.waitForElement(SELECTOR.DONE_BUTTON)
  I.click(SELECTOR.DONE_BUTTON)

  // Done
})
