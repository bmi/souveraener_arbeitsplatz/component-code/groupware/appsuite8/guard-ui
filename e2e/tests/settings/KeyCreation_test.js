/**
 * I work is provided under the terms of the CREATIVE COMMONS PUBLIC
 * LICENSE. I work is protected by copyright and/or other applicable
 * law. Any use of the work other than as authorized under I license
 * or copyright law is prohibited.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 * © 2017 OX Software GmbH, Germany. info@open-xchange.com
 *
 * @author Greg Hill <greg.hill@open-xchange.com>
 */

/// <reference path="../../steps.d.ts" />

const SELECTOR = require('../../constants.js')

Feature('Key Creation')

Before(async function ({ users }) {
  await Promise.all([
    users.create(),
    users.create()
  ])
})

After(async function ({ users }) {
  await users.removeAll()
})

async function doSetup (I, o) {
  I.waitForVisible('.wizard-content')
  I.see('Welcome to Guard', '.wizard-content')
  I.click('Start Setup')

  I.waitForVisible('.password')
  I.waitForVisible('#newogpassword')
  I.insertCryptPassword('#newogpassword', o)
  I.wait(1)
  I.insertCryptPassword('#newogpassword2', o)
  I.click('Next')

  I.waitForVisible('.wizard-title')
  I.see('Guard set up completed', '.wizard-title')
  I.click('Close')
}

Scenario('Create keys from within compose', async function ({ I, users }) {
  const o1 = {
    user: users[0]
  }

  I.login('app=io.ox/mail', o1)

  I.waitForVisible('.io-ox-mail-window .window-body .classic-toolbar')

  I.click('New email')
  I.waitForVisible(SELECTOR.WINDOW_BUSY)
  I.waitForInvisible(SELECTOR.WINDOW_BUSY)
  I.waitForVisible(SELECTOR.EDITOR)

  I.click('.toggle-encryption')

  doSetup(I, o1)
  I.auth(o1)

  I.waitForVisible(SELECTOR.GUARD_LOCK)
})

Scenario('Create keys from within drive', async function ({ I, users }) {
  const o = {
    user: users[0]
  }

  const infostoreFolderID = await I.grabDefaultFolder('infostore')
  const fileId = await I.haveFile(infostoreFolderID, 'testFiles/simpleText.txt', o)
  const id = fileId.folder_id + '.' + fileId.id

  I.login('app=io.ox/files', o)

  // Start to encrypt the file
  I.waitForVisible('.list-view li[data-cid="' + id + '"]')
  I.click('.list-view li[data-cid="' + id + '"]')
  I.wait(1)
  I.click(SELECTOR.FILE_MORE)
  I.click('Encrypt')

  doSetup(I, o)

  I.waitForVisible(SELECTOR.GUARD_FILE_SELECTOR)
  I.see('.txt.pgp', '.extension')
  I.logout()
})

Scenario('Key creation in setup', async function ({ I, users }) {
  const o = {
    user: users[0]
  }

  // Log in to settings
  I.login(['app=io.ox/settings', 'folder=virtual/settings/io.ox/guard'], o)

  I.waitForElement('.io-ox-guard-settings')
  I.waitForElement('.guard-start-button')
  I.wait(1) // Let background loading finish
  // Click the setup button and verify wizard;
  I.click('.guard-start-button')

  doSetup(I, o)

  // Verify settings now shown
  I.waitForVisible('.btn#changePassword')
})
