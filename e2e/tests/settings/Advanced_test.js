/**
 * This work is provided under the terms of the CREATIVE COMMONS PUBLIC
 * LICENSE. This work is protected by copyright and/or other applicable
 * law. Any use of the work other than as authorized under this license
 * or copyright law is prohibited.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 * © 2017 OX Software GmbH, Germany. info@open-xchange.com
 *
 * @author Greg Hill <greg.hill@open-xchange.com>
 */

/// <reference path="../../steps.d.ts" />

const SELECTOR = require('../../constants.js')

Feature('Basic and Advanced Settings Tests')

Before(async function ({ users }) {
  await users.create()
})

After(async function ({ users }) {
  await users.removeAll()
})

Scenario('Basic user only sees basic settings', async function ({ I, users, settings }) {
  const o = {
    user: users[0]
  }

  await I.login('app=io.ox/mail', o)

  await I.verifyUserSetup(o)

  // Next, log in to settings
  await settings.goToSettings()

  I.waitForElement('.io-ox-guard-settings')
  I.waitForElement('input[name="advanced"]')
  I.dontSee('Autocrypt Keys')
  I.dontSee('Your keys')
  I.dontSee('Public keys of recipients')
  I.dontSee('Autocrypt')
  I.dontSee('Autocrypt transfer keys')
})

Scenario('Basic user only sees basic email wording', async function ({ I, users }) {
  const o = {
    user: users[0]
  }

  I.login('app=io.ox/mail', o)

  await I.verifyUserSetup(o)
  I.waitForVisible('.io-ox-mail-window .leftside')
  I.startEncryptedEmail(o)
  I.waitForVisible('.info-line')
  I.waitForVisible(SELECTOR.SECURITY_OPTIONS)
  I.click(SELECTOR.SECURITY_OPTIONS)
  I.see('Sign email')
  I.dontSee('PGP Mime')
  I.dontSee('PGP Inline')
  // TODO: should action really not be available for basic users?
  // I.dontSee('Attach my public key');
  I.click('.smart-dropdown-container')

  I.click(SELECTOR.SAVE_AND_CLOSE)
})

Scenario('Advanced user only sees advanced settings', async function ({ I, users, settings }) {
  const o = {
    user: users[0]
  }

  await I.setupUser(o, true) // need advanced settings

  I.login('app=io.ox/mail', o)

  await I.verifyUserSetup(o)

  // Next, log in to settings
  await settings.goToSettings()

  I.waitForElement('.io-ox-guard-settings')
  I.waitForElement('input[name="advanced"]')
  I.see('Autocrypt Keys')
  I.see('Your keys')
  I.see('Public keys of recipients')
  I.see('Autocrypt')
  I.see('Autocrypt transfer keys')
})

Scenario('Advanced user sees advanced email wording', async function ({ I, users }) {
  const o = {
    user: users[0]
  }

  await I.setupUser(o, true) // need advanced settings

  I.login('app=io.ox/mail', o)

  await I.verifyUserSetup(o)
  I.waitForVisible('.io-ox-mail-window .leftside')
  I.startEncryptedEmail(o)

  I.waitForVisible('.info-line')
  I.waitForVisible(SELECTOR.SECURITY_OPTIONS)
  I.click(SELECTOR.SECURITY_OPTIONS)

  I.see('Security')
  I.see('Sign email')
  I.see('Attach my public key')

  I.see('PGP Format')
  I.see('Mime')
  I.see('Inline')

  I.click('.smart-dropdown-container')
  I.click(SELECTOR.SAVE_AND_CLOSE)
})
