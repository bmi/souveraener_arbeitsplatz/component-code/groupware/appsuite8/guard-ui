/**
 * This work is provided under the terms of the CREATIVE COMMONS PUBLIC
 * LICENSE. This work is protected by copyright and/or other applicable
 * law. Any use of the work other than as authorized under this license
 * or copyright law is prohibited.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 * © 2017 OX Software GmbH, Germany. info@open-xchange.com
 *
 * @author Greg Hill <greg.hill@open-xchange.com>
 */

/// <reference path="../../steps.d.ts" />
const SELECTOR = require('../../constants')

Feature('Office: Create encrypted documents')

// const SELECTOR = require('../../constants')

Before(async function ({ users }) {
  await users.create()
})

After(async function ({ users }) {
  await users.removeAll()
})

Scenario('Office: Create an encrypted text document', async function ({ I, users }) {
  if (await I.amDisabled('office')) return
  const o = {
    user: users[0]
  }

  await I.setupUser(o, true)
  await I.haveSetting('io.ox/office//isCheckSpellingPermanently', false, o)
  await o.user.hasCapability('guard-docs')

  I.login('app=io.ox/files', o)

  await I.verifyUserSetup(o)
  I.wait(1)
  I.waitForVisible(SELECTOR.NEW_BUTTON)
  I.click(SELECTOR.NEW_BUTTON)
  I.waitForVisible('.smart-dropdown-container [data-name="text-newblank"]')
  I.click('.smart-dropdown-container [data-name="text-newblank"]')

  I.wait(3)

  // Switch to office tab and add test text
  I.switchToNextTab()
  I.waitForVisible('.button[data-value="format"]', 60)
  I.waitForVisible('.page.formatted-content.user-select-text')
  I.wait(1)
  I.addToTextDocument('test')

  // Save as encrypted file
  I.click('.button[data-value="file"]')
  I.waitForVisible('.group[data-key="view/saveas/menu"]')
  I.wait(1)
  I.click('.group[data-key="view/saveas/menu"]')
  I.click('Save as (encrypted)')
  I.waitForVisible('#save-as-filename')
  I.wait(1)
  I.fillField('#save-as-filename', 'testTextEncr')
  I.click('Save')

  // Enter Guard password
  I.auth(o)

  // Office closes and re-opens.  Wait
  I.wait(5)
  I.waitForVisible('.page.formatted-content.user-select-text')

  // Close the office tab, find file in drive and click
  I.closeCurrentTab()
  I.waitForVisible('.visible-selection')
  I.click('.apptitle[aria-label="Refresh"]')
  I.wait(2)
  I.see('.docx.pgp', '.extension')
  I.doubleClick(SELECTOR.GUARD_FILE_SELECTOR_DOCX)

  // Lets check we can read it
  I.auth(o)

  I.wait(2)
  I.switchToNextTab()

  I.waitForVisible('.page.formatted-content.user-select-text')
  I.waitForVisible('.button[data-value="format"]', 60)
  I.wait(1)
  I.see('test', '.page.formatted-content.user-select-text')

  // Closeup
  I.closeCurrentTab()
  I.wait(2)
  I.logout()
})
