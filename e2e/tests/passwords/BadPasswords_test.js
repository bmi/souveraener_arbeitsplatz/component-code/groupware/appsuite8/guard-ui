/**
 * This work is provided under the terms of the CREATIVE COMMONS PUBLIC
 * LICENSE. This work is protected by copyright and/or other applicable
 * law. Any use of the work other than as authorized under this license
 * or copyright law is prohibited.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 * © 2017 OX Software GmbH, Germany. info@open-xchange.com
 *
 * @author Greg Hill <greg.hill@open-xchange.com>
 */

/// <reference path="../../steps.d.ts" />

Feature('Bad Passwords')

const SELECTOR = require('../../constants')

Before(async function ({ users }) {
  await users.create()
})

After(async function ({ users }) {
  await users.removeAll()
})

function createBadUser () {
  return {
    user: {
      userdata: {
        password: 'failme'
      }
    }
  }
}

Scenario('Bad password tests decrypting email', async function ({ I, users }) {
  const o = {
    user: users[0]
  }
  const user = o.user

  I.login('app=io.ox/mail', o)

  await I.verifyUserSetup(o)

  await I.haveMail({
    attachments: [{
      content: 'Test content ',
      content_type: 'text/html',
      disp: 'inline'
    }],
    from: [[user.get('display_name'), user.get('primaryEmail')]],
    sendtype: 0,
    subject: 'Test subject ',
    to: [[user.get('display_name'), user.get('primaryEmail')]],
    security: {
      encrypt: true,
      sign: false
    }
  })

  I.waitForElement(SELECTOR.REFRESH_SPIN)
  I.waitForDetached(SELECTOR.REFRESH_SPIN)
  I.click(SELECTOR.REFRESH)

  I.waitForVisible('.list-item.unread')

  I.click('.list-item.unread') // click email

  I.waitForElement(SELECTOR.PASSWORD_PROMPT)
  I.insertCryptPassword(SELECTOR.PASSWORD_PROMPT, createBadUser())
  I.click(SELECTOR.GUARD_PASS_BUTTON)

  I.waitForVisible('.io-ox-alert-error')
  I.see('Bad password', '.io-ox-alert-error')
})

Scenario('Bad password test file', async function ({ I, users }) {
  const o = {
    user: users[0],
    cryptoAction: 'encrypt'
  }

  I.login('app=io.ox/files', o)

  await I.verifyUserSetup(o)

  const infostoreFolderID = await I.grabDefaultFolder('infostore')
  const fileId = await I.haveFile(infostoreFolderID, 'testFiles/simpleText.txt', o)
  const id = fileId.folder_id + '.' + fileId.id
  I.waitForElement(SELECTOR.REFRESH_SPIN)
  I.waitForDetached(SELECTOR.REFRESH_SPIN)
  I.click(SELECTOR.REFRESH)
  // Encrypt the file
  I.waitForVisible('.list-view li[data-cid="' + id + '"]')
  I.doubleClick('.list-view li[data-cid="' + id + '"]')

  I.auth(createBadUser())

  I.waitForVisible('.io-ox-alert-error')
  I.see('Bad password', '.io-ox-alert-error')
})

Scenario('Lockout test', async function ({ I, users }) {
  const o = {
    user: users[0]
  }
  const user = o.user

  I.login('app=io.ox/mail', o)

  await I.verifyUserSetup(o)

  await I.haveMail({
    attachments: [{
      content: 'Test content ',
      content_type: 'text/html',
      disp: 'inline'
    }],
    from: [[user.get('display_name'), user.get('primaryEmail')]],
    sendtype: 0,
    subject: 'Test subject ',
    to: [[user.get('display_name'), user.get('primaryEmail')]],
    security: {
      encrypt: true,
      sign: false
    }
  })

  I.waitForVisible('.io-ox-mail-window .classic-toolbar-container .classic-toolbar')
  I.waitForElement(SELECTOR.REFRESH_SPIN)
  I.waitForDetached(SELECTOR.REFRESH_SPIN)
  I.click(SELECTOR.REFRESH)

  I.waitForVisible('.list-item.unread')

  I.click('.list-item.unread') // click email

  I.waitForElement(SELECTOR.PASSWORD_PROMPT)
  I.insertCryptPassword(SELECTOR.PASSWORD_PROMPT, createBadUser())
  I.click(SELECTOR.GUARD_PASS_BUTTON)

  I.waitForVisible('.io-ox-alert-error')
  I.see('Bad password', '.io-ox-alert-error')
  for (let i = 0; i < 6; i++) {
    I.click('.io-ox-alert-error .btn')
    I.wait(1)
    I.click(SELECTOR.GUARD_PASS_BUTTON)
    I.waitForVisible('.io-ox-alert-error')
  }

  I.see('Lockout', '.io-ox-alert-error')
})
