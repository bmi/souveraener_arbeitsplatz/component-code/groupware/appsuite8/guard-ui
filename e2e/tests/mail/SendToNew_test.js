/**
 * This work is provided under the terms of the CREATIVE COMMONS PUBLIC
 * LICENSE. This work is protected by copyright and/or other applicable
 * law. Any use of the work other than as authorized under this license
 * or copyright law is prohibited.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 * © 2017 OX Software GmbH, Germany. info@open-xchange.com
 *
 * @author Greg Hill <greg.hill@open-xchange.com>
 */

/// <reference path="../../steps.d.ts" />

Feature('Send Encrypted To New Guard User')

const SELECTOR = require('../../constants')

Before(async function ({ users }) {
  await Promise.all([
    users.create(),
    users.create()
  ])
})

After(async function ({ users }) {
  await users.removeAll()
})

async function ComposeRecieve (I, users) {
  const o1 = {
    user: users[0]
  }
  const o2 = {
    user: users[1]
  }

  await I.setupUser(o1, true)
  await I.setupUser(o2)

  I.login('app=io.ox/mail', o1)

  I.waitForVisible('.io-ox-mail-window .window-body .classic-toolbar')

  I.verifyUserSetup(o1) // Verify user has Guard setup

  // Test data
  const random = Math.floor((Math.random() * 100000))
  const subject = 'Test Subject ' + random
  const data = 'Test data ' + random

  // Open compose
  I.sendEmail(o2, subject, data)

  I.wait(2)

  // Logout and log back in as second user
  I.logout()
  I.login('app=io.ox/mail', o2)

  I.waitForVisible('.io-ox-mail-window .leftside')
  I.waitForVisible(SELECTOR.UNREAD)
  I.click(SELECTOR.UNREAD)

  // Change temporary password
  I.changeTemporaryPassword(o2)

  I.waitForVisible(SELECTOR.DONE) // Wizard shows done
  I.click(SELECTOR.DONE)

  // Verify decrypted
  I.verifyDecryptedMail(subject, data)

  // OK, done
  I.logout()
}

Scenario('Desktop: Compose and receive encrypted email, database composition space', async function ({ I, users }) {
  await users[0].hasConfig('com.openexchange.mail.compose.mailstorage.enabled', false)
  await ComposeRecieve(I, users)
})

Scenario('Desktop: Compose and receive encrypted email, default composition space', async function ({ I, users }) {
  await ComposeRecieve(I, users)
})

// Testing sending in mobile views
async function sendMobile (I, users, device, landscape) {
  const o1 = {
    user: users[0]
  }
  const o2 = {
    user: users[1]
  }

  await I.setupUser(o1, true)
  await I.setupUser(o2)

  await I.openMobile(device, landscape)

  I.login('app=io.ox/mail', o1)

  I.waitForVisible('.inline-toolbar [data-action="io.ox/mail/actions/compose"]')

  I.verifyUserSetup(o1) // Verify user has Guard setup

  // Test data
  const random = Math.floor((Math.random() * 100000))
  const subject = 'Test Subject ' + random
  const data = 'Test data ' + random

  // Open compose
  I.sendMobileEmail(o2, subject, data)

  I.wait(2)

  // Logout and log back in as second user
  I.logout()
  I.login('app=io.ox/mail', o2)

  I.waitForVisible('.list-item.unread')
  I.tap('.unread .list-item-content')

  // Change temporary password
  I.changeTemporaryPassword(o2)

  // Verify decrypted
  I.verifyDecryptedMail(subject, data)

  I.waitForVisible('.io-ox-alert')
  I.waitForInvisible('.io-ox-alert')

  // OK, done
  I.logout()
}

// Test with Android in landscape
Scenario('Mobile: Android (Landscape) - Compose and receive encrypted email', async function ({ I, users }) {
  await sendMobile(I, users, 'Pixel 2', true)
})

// Test with large iphone size
Scenario('Mobile: Iphone X - Compose and receive encrypted email', async function ({ I, users }) {
  await sendMobile(I, users, 'iPhone X')
})
