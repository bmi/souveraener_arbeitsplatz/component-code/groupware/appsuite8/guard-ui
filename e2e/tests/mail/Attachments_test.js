/**
 * This work is provided under the terms of the CREATIVE COMMONS PUBLIC
 * LICENSE. This work is protected by copyright and/or other applicable
 * law. Any use of the work other than as authorized under this license
 * or copyright law is prohibited.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 * © 2017 OX Software GmbH, Germany. info@open-xchange.com
 *
 * @author Greg Hill <greg.hill@open-xchange.com>
 */

/// <reference path="../../steps.d.ts" />

Feature('Attachments test')

const SELECTOR = require('../../constants')

Before(async function ({ users }) {
  await Promise.all([
    users.create(),
    users.create()
  ])
})

After(async function ({ users }) {
  await users.removeAll()
})

async function sendToSelf (I, o, att) {
  const random = Math.floor((Math.random() * 100000))
  const subject = 'Test Subject ' + random
  const data = 'Test data ' + random

  // Mark inbox as all read
  I.selectFolder('Inbox')
  I.waitForVisible(SELECTOR.FOLDER_CONTEXT_MENU)
  I.click(SELECTOR.FOLDER_CONTEXT_MENU)
  I.waitForVisible(SELECTOR.FOLDER_CONTEXT_MENU_MARK_READ)
  I.click(SELECTOR.FOLDER_CONTEXT_MENU_MARK_READ)

  // Open compose
  I.sendEmail(o, subject, data, att)

  I.waitForVisible('.io-ox-mail-window .leftside')
  I.click(SELECTOR.REFRESH)
  I.waitForVisible(SELECTOR.UNREAD)
  I.click(SELECTOR.UNREAD)
  I.waitForVisible(SELECTOR.DETAIL_SUBJECT)
  I.see(subject)

  // Decrypt
  I.waitForElement(SELECTOR.GUARD_PASS_BUTTON)
  I.waitForElement(SELECTOR.PASSWORD_PROMPT)
  I.insertCryptPassword(SELECTOR.PASSWORD_PROMPT, o)
  I.click(SELECTOR.GUARD_PASS_BUTTON)

  I.verifyDecryptedMail(subject, data)
}

Scenario('View attachment', async function ({ I, users }) {
  const o = {
    user: users[1]
  }

  await I.setupUser(o, true)

  // Log in as User 0
  I.login('app=io.ox/mail', o)

  I.waitForVisible('.io-ox-mail-window .window-body')

  I.verifyUserSetup(o) // Verify user has Guard setup

  await sendToSelf(I, o, 'testFiles/ox.png')

  // Verify Attachment
  I.waitForVisible(SELECTOR.DETAIL_VIEW)
  I.click(SELECTOR.DETAIL_VIEW)

  // Verify password prompt
  I.auth(o)

  // Verify attachment viewable
  I.waitForVisible(SELECTOR.VIEWER_IMAGE)

  // Close it
  I.click(SELECTOR.VIEWER_CLOSE)
  I.waitForVisible('.io-ox-mail-window .leftside')

  // OK, done
  I.logout()
})

Scenario('Save attachment', async function ({ I, users }) {
  const o = {
    user: users[0]
  }

  await I.setupUser(o, true)

  // Log in as User 0
  I.login('app=io.ox/mail', o)

  I.waitForVisible('.io-ox-mail-window .window-body')

  I.verifyUserSetup(o) // Verify user has Guard setup

  await sendToSelf(I, o, 'testFiles/ox.png')

  // Verify Attachment
  I.waitForVisible(SELECTOR.DETAIL_SAVE)
  I.click(SELECTOR.DETAIL_SAVE)

  // Verify password prompt
  I.auth(o)

  // Verify attachment viewable
  I.waitForVisible('.folder-picker-dialog')
  I.waitForVisible('.folder.selectable.open.selected')

  // Close it
  I.click('Save')
  I.waitForInvisible('.io-ox-alert.io-ox-alert-busy')
  I.wait(1)
  I.see('Attachment has been saved')

  // Verify there and can be opened
  I.openApp('Drive')
  I.waitForVisible('ul.file-list-view.complete')
  I.see('ox', '.filename')
  I.see('.png', '.extension')

  I.doubleClick('.file-type-image .list-item-content')

  // Verify attachment viewable
  I.waitForVisible(SELECTOR.VIEWER_IMAGE)
  I.waitForVisible(SELECTOR.VIEWER_FILENAME)
  I.see('ox.png', SELECTOR.VIEWER_FILENAME)

  // Close it
  I.click(SELECTOR.VIEWER_CLOSE)
  I.waitForVisible('ul.file-list-view.complete')

  // OK, done
  I.logout()
})

Scenario('Office: View then edit docx file attachment', async function ({ I, users }) {
  if (await I.amDisabled('office')) return
  const o = {
    user: users[0]
  }

  await I.setupUser(o, true)

  // Log in as User 0
  I.login('app=io.ox/mail', o)

  I.waitForVisible('.io-ox-mail-window .window-body')

  I.verifyUserSetup(o) // Verify user has Guard setup

  await sendToSelf(I, o, 'testFiles/Test.docx')

  // Verify Attachment
  I.waitForVisible(SELECTOR.DETAIL_VIEW)
  I.click(SELECTOR.DETAIL_VIEW)

  // Verify password prompt
  I.auth(o)

  I.waitForVisible('.document-container.io-ox-core-pdf', 20)
  I.waitForText('Test', 3, '.text-wrapper')

  // Close it
  I.click(SELECTOR.VIEWER_CLOSE)
  I.waitForVisible('.io-ox-mail-window .leftside')

  // Verify Attachment
  I.waitForVisible('.inline-toolbar [data-action="io.ox/mail/office/text-edit-asnew"]')
  I.click('.inline-toolbar [data-action="io.ox/mail/office/text-edit-asnew"]')
  // Verify password prompt
  // Change in behaviour?
  // I.auth(o)

  I.wait(2)

  I.switchToNextTab()
  I.waitForVisible('.button[data-value="format"]', 60)
  I.waitForVisible('.page.formatted-content.user-select-text', 20)
  I.see('Test', '.pagecontent')

  I.closeCurrentTab()
  I.wait(1)

  // OK, done
  I.logout()
})

Scenario('Use Drive Mail prevented', async function ({ I, users }) {
  const o = {
    user: users[1]
  }

  await I.setupUser(o, true)

  // Log in as User 0
  I.login('app=io.ox/mail', o)

  I.waitForVisible('.io-ox-mail-window .window-body')

  I.verifyUserSetup(o) // Verify user has Guard setup
  const opt = {
    unencrypted: true,
    startOnly: true
  }
  I.sendEmail(o, 'test', 'test', 'testFiles/ox.png', opt)

  I.waitForVisible('.share-attachments input')
  I.click('.share-attachments')

  I.click('.io-ox-mail-compose-window .toggle-encryption')
  I.waitForVisible('#ogPassword')
  I.fillField('#ogPassword', o.user.userdata.password)
  I.click('[data-point="oxguard_core/auth"] .btn[data-action="ok"]')

  I.see('Not Supported')
  I.click('.btn[data-action="ok"]')

  I.dontSee('.share-attachments')
})
