/**
 * This work is provided under the terms of the CREATIVE COMMONS PUBLIC
 * LICENSE. This work is protected by copyright and/or other applicable
 * law. Any use of the work other than as authorized under this license
 * or copyright law is prohibited.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 * © 2017 OX Software GmbH, Germany. info@open-xchange.com
 *
 * @author Greg Hill <greg.hill@open-xchange.com>
 */

/// <reference path="../../steps.d.ts" />

Feature('Conversation between users')

const SELECTOR = require('../../constants')

Before(async function ({ users }) {
  await Promise.all([
    users.create(),
    users.create()
  ])
})

After(async function ({ users }) {
  await users.removeAll()
})

Scenario('Encrypted conversation between two users', async function ({ I, users }) {
  const o1 = {
    user: users[0]
  }
  const o2 = {
    user: users[1]
  }

  await I.setupUser(o1, true)
  await I.setupUser(o2, true)

  I.login('app=io.ox/mail', o1)

  I.waitForVisible('.io-ox-mail-window .window-body .classic-toolbar')

  I.verifyUserSetup(o1) // Verify user has Guard setup

  // Test data
  const random = Math.floor((Math.random() * 100000))
  const subject = 'Test Subject ' + random
  const data = 'Test data ' + random

  // Open compose
  I.sendEmail(o2, subject, data)

  I.wait(2)

  // Logout and log back in as second user
  I.logout()
  I.login('app=io.ox/mail', o2)

  I.waitForVisible('.io-ox-mail-window .leftside')
  I.waitForVisible(SELECTOR.UNREAD)
  I.click(SELECTOR.UNREAD)

  // Change temporary password
  I.changeTemporaryPassword(o2)

  I.waitForVisible(SELECTOR.DONE) // Wizard shows done
  I.click(SELECTOR.DONE)

  // Verify decrypted
  I.verifyDecryptedMail(subject, data)

  // Reply
  I.click(SELECTOR.REPLY)

  I.auth(o2)

  I.wait(2)
  I.waitForInvisible(SELECTOR.WINDOW_BUSY)
  // Change contents to ReplyTest to verify later
  within({ frame: SELECTOR.EDITOR_IFRAME }, async () => {
    I.see(data, SELECTOR.EMAIL_BODY)
    I.see('wrote:')
    I.click(SELECTOR.EMAIL_BODY)
    I.fillField(SELECTOR.EMAIL_BODY, 'ReplyTest')
  })

  I.waitForVisible(SELECTOR.TRUSTED_KEY)
  // Send
  I.click('Send')

  I.waitForVisible('.io-ox-mail-window .leftside')
  I.wait(2)
  I.logout()

  // Switch back to first user
  I.login('app=io.ox/mail', o1)
  I.waitForVisible(SELECTOR.UNREAD)
  // Conversation view
  I.click(SELECTOR.GRID_OPTIONS)
  I.click('Conversations')
  I.wait(1)

  // Read first
  I.waitForVisible(SELECTOR.UNREAD)
  I.click(SELECTOR.UNREAD)
  I.waitForElement(SELECTOR.GUARD_PASS_BUTTON)
  I.waitForElement(SELECTOR.PASSWORD_PROMPT)
  I.insertCryptPassword(SELECTOR.PASSWORD_PROMPT, o1)
  I.click(SELECTOR.GUARD_PASS_BUTTON)

  I.verifyDecryptedMail(subject, 'ReplyTest')

  // Check the original is still there in the conversation and decryptable
  I.click('.thread-view article:nth-of-type(2) .toggle-mail-body')
  I.waitForElement(SELECTOR.PASSWORD_PROMPT)
  I.insertCryptPassword(SELECTOR.PASSWORD_PROMPT, o1)
  I.click(SELECTOR.GUARD_PASS_BUTTON)
  I.wait(1)
  I.switchTo('.thread-view article:nth-of-type(2) iframe')
  I.see(data)
  I.switchTo()
  I.wait(1)

  I.logout()
})
