import { defineConfig } from 'vite'
import { createHtmlPlugin } from 'vite-plugin-html'
import vitePluginOxManifests from '@open-xchange/vite-plugin-ox-manifests'
import vitePluginOxExternals from '@open-xchange/vite-plugin-ox-externals'
import vitePluginProxy from '@open-xchange/vite-plugin-proxy'
import gettextPlugin from '@open-xchange/rollup-plugin-po2json'
import { config } from 'dotenv-defaults'

config()

const PROXY_URL = new URL(process.env.SERVER)
const FRONTEND_URIS = process.env.FRONTEND_URIS || ''
const ENABLE_STRICT_FILESYSTEM = process.env.ENABLE_STRICT_FILESYSTEM === 'true'
const ENABLE_HTTP_PROXY = process.env.ENABLE_HTTP_PROXY === 'true'
const ENABLE_SECURE_FRONTENDS = process.env.ENABLE_SECURE_FRONTENDS === 'true'
const ENABLE_SECURE_PROXY = process.env.ENABLE_SECURE_PROXY === 'true'
const PORT = process.env.PORT
const ENABLE_HMR = process.env.ENABLE_HMR === 'true'

export default defineConfig({
  root: './src',
  base: './',
  publicDir: '../public',
  build: {
    minify: 'esbuild',
    target: 'es2020',
    outDir: '../dist',
    emptyOutDir: true,
    polyfillDynamicImport: false,
    sourcemap: true,
    rollupOptions: {
      input: {},
      preserveEntrySignatures: 'strict',
      output: {
        minifyInternalExports: false,
        entryFileNames: '[name].js'
      }
    }

  },
  server: process.env.HTTP_ONLY === 'true'
    ? {
        port: PORT,
        hmr: {
          overlay: ENABLE_HMR
        }
      }
    : {
        port: PORT,
        hmr: {
          overlay: ENABLE_HMR
        },
        https: {
          key: process.env.HOST_KEY || 'ssl/host.key',
          cert: process.env.HOST_CRT || 'ssl/host.crt'
        }
      },
  fs: {
    strict: ENABLE_STRICT_FILESYSTEM
  },
  resolve: {
    alias: [
      {
        find: '@/', replacement: '/'
      }
    ]
  },
  optimizeDeps: {
    include: [
      'jquery',
      'backbone',
      'backbone-validation',
      'dompurify'
    ],
    exclude: [
      '@open-xchange/bootstrap',
      'underscore'
    ]
  },
  plugins: [
    createHtmlPlugin(),
    vitePluginProxy({
      proxy: {
        '/api': {
          target: PROXY_URL.href,
          changeOrigin: true,
          secure: ENABLE_SECURE_PROXY
        },
        '/ajax': {
          target: PROXY_URL.href,
          changeOrigin: true,
          secure: ENABLE_SECURE_PROXY
        },
        '/help': {
          target: PROXY_URL.href,
          changeOrigin: true,
          secure: ENABLE_SECURE_PROXY
        },
        '/socket.io/appsuite': {
          target: `wss://${PROXY_URL.host}/socket.io/appsuite`,
          ws: true,
          changeOrigin: true,
          secure: ENABLE_SECURE_PROXY
        },
        '/rt2/v1/default/': {
          target: `wss://${PROXY_URL.host}`,
          ws: true,
          changeOrigin: true,
          secure: ENABLE_SECURE_PROXY
        }
      },
      httpProxy: ENABLE_HTTP_PROXY && {
        target: PROXY_URL.href,
        port: PROXY_URL.port || 8080
      },
      frontends: FRONTEND_URIS && FRONTEND_URIS.split(',').map(uri => ({ target: uri, secure: ENABLE_SECURE_FRONTENDS })),
      secure: false,
      logLevel: process.env.PROXY_LOGLEVEL || 'error'
    }),
    vitePluginOxManifests({
      watch: true,
      entryPoints: ['src/**/*.js'],
      manifestsAsEntryPoints: true,
      meta: {
        id: 'guard-ui',
        name: 'Guard UI',
        buildDate: new Date().toISOString(),
        commitSha: process.env.CI_COMMIT_SHA,
        version: String(process.env.APP_VERSION || '').split('-')[0],
        revision: String(process.env.APP_VERSION || '').split('-')[1]
      }

    }),
    gettextPlugin({
      poFiles: 'src/i18n/*.po',
      outFile: 'ox.pot',
      defaultDictionary: 'io.ox/guard/i18n',
      defaultLanguage: 'en_US'
    }),
    vitePluginOxExternals({
      prefix: '$'
    })
  ]
})
